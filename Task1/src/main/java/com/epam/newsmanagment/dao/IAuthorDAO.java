package com.epam.newsmanagment.dao;

import java.sql.Timestamp;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.exception.DAOException;

public interface IAuthorDAO
{
    public void addNewsAuthorRelation(Long newsId, Long authorId) throws DAOException;
    public void expireAuthor(Long authorId, Timestamp expirationDate) throws DAOException;
    public Long addAuthor(Author author) throws DAOException;
    public void updateAuthor(Author author) throws DAOException;
    public Author getAuthor(Long authorId) throws DAOException; 
}
