package com.epam.newsmanagment.dao.impl;

import static com.epam.newsmanagment.utils.DBUtils.closeResources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.utils.DBUtils;
import com.epam.newsmanagment.utils.SearchCriteriaUtils;

public class NewsDAOImpl implements INewsDAO
{
    private DataSource dataSource;

    /**
     * Creates DAO and tries to access a free connection from
     * {@link ConnectionPool}.
     *
     * @throws ConnectionException
     *             if couldn't get a free connection.
     */
    public NewsDAOImpl(DataSource dataSource)
    {
	super();
	this.dataSource = dataSource;
    }

    /**
     * Adds news to database.
     *
     * @param news
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public Long addNews(News news) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	Long returnId;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(
		    "INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) "
			    + "VALUES (news_seq.nextval, ?, ?, ?, ?, ?)",
		    new String[] { "NEWS_ID" });
	    ps.setString(1, news.getTitle());
	    ps.setString(2, news.getShortText());
	    ps.setString(3, news.getFullText());
	    ps.setTimestamp(4, news.getCreationDate());
	    ps.setDate(5, news.getModificationDate());
	    ps.executeUpdate();

	    rs = ps.getGeneratedKeys();
	    if (!rs.next())
	    {
		throw new DAOException("Can't generate id for new news entity.");
	    }
	    returnId = rs.getLong(1);
	    news.setId(returnId);
	    return returnId;
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in news adding.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    /**
     * Updates news in database
     * 
     * @param news
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public void updateNews(News news) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(
		    "UPDATE news SET title=?, short_text=?, full_text=?, modification_date=? WHERE news_id=?");
	    ps.setString(1, news.getTitle());
	    ps.setString(2, news.getShortText());
	    ps.setString(3, news.getFullText());
	    ps.setDate(4, news.getModificationDate());
	    ps.setLong(5, news.getId());
	    ps.executeUpdate();
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in news editing.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    /**
     * Deletes news from database.
     * 
     * @param news
     *            id
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public void deleteNews(Long newsId) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("DELETE FROM news WHERE news_id=?");
	    ps.setLong(1, newsId);
	    ps.executeUpdate();
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in news deletion.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    /**
     * Returns list of news.
     *
     * @return list of news.
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    /*
     * @Override public List<News> getAllNews() throws DAOException { try
     * (PreparedStatement ps = connection.prepareStatement("SELECT * FROM news"
     * )) { ResultSet rs = ps.executeQuery(); List<News> newsList = new
     * ArrayList<News>(); while (rs.next()) { newsList.add(new
     * News(rs.getLong("news_id"), rs.getString("title"),
     * rs.getString("short_text"), rs.getString("full_text"),
     * rs.getDate("creation_date"), rs.getTimestamp("modification_date"))); }
     * 
     * return newsList; } catch (SQLException e) { throw new DAOException(
     * "Error in getting news list.", e); } }
     */

    /**
     * Returns news by id.
     *
     * @param id
     * @return news.
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public News getNews(Long newsId) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("SELECT * FROM news WHERE news_id=?");
	    ps.setLong(1, newsId);
	    rs = ps.executeQuery();
	    rs.next();
	    return new News(rs.getLong("news_id"), rs.getString("title"), rs.getString("short_text"),
		    rs.getString("full_text"), rs.getTimestamp("creation_date"), rs.getDate("modification_date"));
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in getting news.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    /**
     * Returns list of news suiting to search criteria.
     *
     * @return list of news suiting to search criteria.
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public List<News> searchNews(SearchCriteria searchCriteria) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    List<News> list = new ArrayList<News>();
	    connection = DBUtils.getConnection(dataSource);

	    StringBuffer sb = new StringBuffer(
		    "SELECT news_id, title, short_text, full_text, creation_date, modification_date, counted, rn"
			    + " FROM(SELECT news_id, title, short_text, full_text, creation_date, modification_date, counted, "
			    + "ROWNUM rn FROM (SELECT searched.news_id, searched.title, searched.short_text, "
			    + "searched.full_text, searched.creation_date, searched.modification_date, searched.counted, "
			    + "ROWNUM rn FROM (SELECT n.news_id, n.title, n.short_text, "
			    + "n.full_text, n.creation_date, n.modification_date, news_comments.counted FROM news n "
			    + "LEFT JOIN (SELECT COUNT(c.comment_id) AS counted, c.news_id FROM comments c "
			    + "INNER JOIN news n ON c.news_id = n.news_id "
			    + "group by C.NEWS_ID) NEWS_COMMENTS on N.NEWS_ID = NEWS_COMMENTS.NEWS_ID ");
	    if (searchCriteria == null)
	    {
		searchCriteria = new SearchCriteria();
	    }
	    sb.append(SearchCriteriaUtils.buildAuthorCriteria(searchCriteria));
	    sb.append(SearchCriteriaUtils.buildTagsCriteria(searchCriteria));
	    sb.append(
		    ") SEARCHED order by SEARCHED.COUNTED desc nulls last, SEARCHED.MODIFICATION_DATE desc, SEARCHED.NEWS_ID asc) "
			    + ") ");

	    ps = connection.prepareStatement(sb.toString());

	    int tagsCount = 0;
	    if (searchCriteria.getTagIds() != null)
	    {
		tagsCount = searchCriteria.getTagIds().size();
	    }
	    int shift = 0;
	    if (searchCriteria.getAuthorId() != null && !((Long) (0L)).equals(searchCriteria.getAuthorId()))
	    {
		shift++;
		ps.setLong(shift, searchCriteria.getAuthorId());
	    }
	    if (tagsCount != 0)
	    {
		for (int i = 1; i <= tagsCount; i++)
		{
		    ps.setLong(i + shift, searchCriteria.getTagIds().get(i - 1));
		}
		ps.setInt(tagsCount + shift + 1, tagsCount);
		shift++;
	    }

	    rs = ps.executeQuery();
	    while (rs.next())
	    {
		list.add(new News(rs.getLong("news_id"), rs.getString("title"), rs.getString("short_text"),
			rs.getString("full_text"), rs.getTimestamp("creation_date"), rs.getDate("modification_date")));
	    }
	    return list;
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in getting news.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    /**
     * Counts all news.
     * 
     * @return number of all news.
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public Integer countAllNews() throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("SELECT COUNT(*) FROM news");
	    rs = ps.executeQuery();
	    rs.next();
	    return rs.getInt(1);
	}
	catch (SQLException e)
	{
	    System.out.println(e);
	    throw new DAOException("Error in counting news.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }
}
