package com.epam.newsmanagment.dao.impl;

import static com.epam.newsmanagment.utils.DBUtils.closeResources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.utils.DBUtils;

public class TagDAOImpl implements ITagDAO
{
    private DataSource dataSource;
    
    public TagDAOImpl(DataSource dataSource)
    {
	super();
	this.dataSource = dataSource;
    }

    /**
     * Adds news-tag relations to database.
     *
     * @param newsId
     * @param tagIdList
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public void addNewsTagsRelations(Long newsId, List<Long> tagIdList) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    
	    StringBuffer buffer = new StringBuffer("INSERT ALL ");
	    for (int i = 0; i < tagIdList.size(); i++)
	    {
		buffer.append("INTO news_tags (news_id, tag_id) VALUES (?, ?) ");
	    }
	    buffer.append("SELECT * FROM dual");
	    ps = connection.prepareStatement(buffer.toString());
	    
	    for (int i = 0; i < tagIdList.size(); i++)
	    {
		ps.setLong(i * 2 + 1, newsId);
		ps.setLong(i * 2 + 2, tagIdList.get(i));
	    }

	    ps.executeUpdate();
	}
	catch (SQLException e)
	{
	    System.out.println(e.getMessage());
	    throw new DAOException("Error in adding news-tag relations.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    @Override
    public Long addTag(Tag tag) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	Long returnId;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(
		    "INSERT INTO tags (tag_id, tag_name) VALUES (tags_seq.nextval, ?)",
		    new String[] { "TAG_ID" });
	    ps.setString(1, tag.getName());
	    ps.executeUpdate();

	    rs = ps.getGeneratedKeys();
	    if (!rs.next())
	    {
		throw new DAOException("Can't generate id for new news entity.");
	    }
	    returnId = rs.getLong(1);
	    tag.setId(returnId);
	    return returnId;
	}
	catch (SQLException e)
	{
	    System.out.println(e.getMessage());
	    throw new DAOException("Error in tag adding.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    @Override
    public void updateTag(Tag tag) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(
		    "UPDATE tags SET tag_name=? WHERE tag_id=?");
	    ps.setString(1, tag.getName());
	    ps.setLong(2, tag.getId());
	    ps.executeUpdate();
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in tag editing.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    @Override
    public Tag getTag(Long tagId) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("SELECT * FROM tags WHERE tag_id=?");
	    ps.setLong(1, tagId);
	    rs = ps.executeQuery();
	    rs.next();
	    
	    return new Tag(rs.getLong("tag_id"), rs.getString("tag_name"));
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in getting tag.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }
}
