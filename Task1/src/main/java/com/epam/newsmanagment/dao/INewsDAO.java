package com.epam.newsmanagment.dao;

import java.util.List;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.DAOException;

public interface INewsDAO
{
    public Long addNews(News news) throws DAOException;
    public News getNews(Long newsId) throws DAOException;
    //public List<News> getAllNews() throws DAOException;
    public void updateNews(News news) throws DAOException;
    public void deleteNews(Long newsId) throws DAOException;
    public List<News> searchNews(SearchCriteria searchCriteria) throws DAOException;
    public Integer countAllNews() throws DAOException;
}
