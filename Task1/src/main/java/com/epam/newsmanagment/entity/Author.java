package com.epam.newsmanagment.entity;

import java.util.Date;

public class Author
{
    private Long id;
    private String name;
    private Date expired;

    public Long getId()
    {
	return id;
    } 

    public void setId(Long id)
    {
	this.id = id;
    }

    public String getName()
    {
	return name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

    public Date getExpired()
    {
	return expired;
    }

    public void setExpired(Date expired)
    {
	this.expired = expired;
    }

    public Author(Long id, String name, Date expired)
    {
	super();
	this.id = id;
	this.name = name;
	this.expired = expired;
    }

    public Author()
    {
	super();
    }

    @Override
    public int hashCode()
    {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((expired == null) ? 0 : expired.hashCode());
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj)
    {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Author other = (Author) obj;
	if (expired == null)
	{
	    if (other.expired != null)
		return false;
	}
	else if (!expired.equals(other.expired))
	    return false;
	if (id == null)
	{
	    if (other.id != null)
		return false;
	}
	else if (!id.equals(other.id))
	    return false;
	if (name == null)
	{
	    if (other.name != null)
		return false;
	}
	else if (!name.equals(other.name))
	    return false;
	return true;
    }

    @Override
    public String toString()
    {
	StringBuilder builder = new StringBuilder();
	builder.append("Author [id=");
	builder.append(id);
	builder.append(", name=");
	builder.append(name);
	builder.append(", expired=");
	builder.append(expired);
	builder.append("]");
	return builder.toString();
    }
}
