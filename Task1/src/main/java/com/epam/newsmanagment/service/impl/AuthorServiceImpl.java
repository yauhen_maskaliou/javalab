package com.epam.newsmanagment.service.impl;

import java.sql.Timestamp;
import org.apache.log4j.Logger;

import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.IAuthorService;

public class AuthorServiceImpl implements IAuthorService
{
    private IAuthorDAO authorDAO;
    private final static Logger LOG = Logger.getLogger(AuthorServiceImpl.class);
    
    public AuthorServiceImpl(IAuthorDAO authorDAO)
    {
	super();
	this.authorDAO = authorDAO;
    }
    
    @Override
    public void addNewsAuthorRelation(Long newsId, Long authorId) throws ServiceException
    {
	try
	{
	    authorDAO.addNewsAuthorRelation(newsId, authorId);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not add news-author relation.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public  void expireAuthor(Long authorId, Timestamp expirationDate) throws ServiceException
    {
	try
	{
	    authorDAO.expireAuthor(authorId, expirationDate);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not expire author.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public Long addAuthor(Author author) throws ServiceException
    {
	try
	{
	    return authorDAO.addAuthor(author);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not add author.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public void updateAuthor(Author author) throws ServiceException
    {
	try
	{
	    authorDAO.updateAuthor(author);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not update author.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public Author getAuthor(Long authorId) throws ServiceException
    {
	try
	{
	    return authorDAO.getAuthor(authorId);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not get author.");
	    throw new ServiceException(e);
	}
    }
}
