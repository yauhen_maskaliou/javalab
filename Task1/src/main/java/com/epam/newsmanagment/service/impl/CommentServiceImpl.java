package com.epam.newsmanagment.service.impl;

import org.apache.log4j.Logger;

import com.epam.newsmanagment.dao.ICommentDAO;
import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.ICommentService;

public class CommentServiceImpl implements ICommentService
{
    private ICommentDAO commentDAO;
    private final static Logger LOG = Logger.getLogger(CommentServiceImpl.class);
    
    public CommentServiceImpl(ICommentDAO commentDAO)
    {
	super();
	this.commentDAO = commentDAO;
    }
    
    @Override
    public Long addComment(Comment comment) throws ServiceException
    {
	try
	{
	    return commentDAO.addComment(comment);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not add comment.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public void deleteComment(Long commentId) throws ServiceException
    {
	try
	{
	    commentDAO.deleteComment(commentId);;
	}
	catch (DAOException e)
	{
	    LOG.error("Can not delete comment.");
	    throw new ServiceException(e);
	}
    }
    
    @Override
    public Comment getComment(Long commentId) throws ServiceException
    {
	try
	{
	    return commentDAO.getComment(commentId);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not delete comment.");
	    throw new ServiceException(e);
	}
    }
}
