package com.epam.newsmanagment.service;

import java.util.List;

import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.exception.ServiceException;

public interface ITagService
{
    public void addNewsTagsRelations(Long newsId, List<Long> tagIdList) throws ServiceException;
    public Long addTag(Tag tag) throws ServiceException;
    public void updateTag(Tag tag) throws ServiceException;
    public Tag getTag(Long tagId) throws ServiceException;
}
