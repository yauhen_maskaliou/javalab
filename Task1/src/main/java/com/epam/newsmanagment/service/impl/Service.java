package com.epam.newsmanagment.service.impl;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Service
{
    private ApplicationContext context;
    
    public Service()
    {
	context = new ClassPathXmlApplicationContext("AuthorDAO.xml");
    }

    public ApplicationContext getContext()
    {
        return context;
    }

    public void setContext(ApplicationContext context)
    {
        this.context = context;
    }
}
