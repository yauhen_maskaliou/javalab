package com.epam.newsmanagment.service;

import java.util.List;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.ServiceException;

public interface INewsService
{
    public void addNews(News news, Long authorId, List<Long> tagIds) throws ServiceException;
    public News getNews(Long newsId) throws ServiceException;
    //public List<News> getAllNews() throws ServiceException;
    public void updateNews(News news) throws ServiceException;
    public void deleteNews(Long newsId) throws ServiceException;
    public List<News> searchNews(SearchCriteria searchCriteria) throws ServiceException;
    public Integer countAllNews() throws ServiceException;
}
