package com.epam.newsmanagment.service;

import java.sql.Timestamp;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.exception.ServiceException;

public interface IAuthorService
{
    public void addNewsAuthorRelation(Long newsId, Long authorId) throws ServiceException;
    public void expireAuthor(Long authorId, Timestamp expirationDate) throws ServiceException;
    public Long addAuthor(Author author) throws ServiceException;
    public void updateAuthor(Author author) throws ServiceException;
    public Author getAuthor(Long authorId) throws ServiceException; 
}
