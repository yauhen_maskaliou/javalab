package com.epam.newsmanagment.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.INewsService;

public class NewsServiceImpl implements INewsService
{
    private INewsDAO newsDAO;
    private IAuthorDAO authorDAO;
    private ITagDAO tagDAO;
    private final static Logger LOG = Logger.getLogger(NewsServiceImpl.class);
    
    public NewsServiceImpl(INewsDAO newsDAO, IAuthorDAO authorDAO, ITagDAO tagDAO)
    {
	super();
	this.newsDAO = newsDAO;
	this.authorDAO = authorDAO;
	this.tagDAO = tagDAO;
    }
    
    public void addNews(News news, Long authorId, List<Long> tagIds) throws ServiceException
    {
	try
	{
	    newsDAO.addNews(news);
	    authorDAO.addNewsAuthorRelation(news.getId(), authorId);
	    tagDAO.addNewsTagsRelations(news.getId(), tagIds);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not add news.");
	    throw new ServiceException(e);
	}
    }

    public News getNews(Long newsId) throws ServiceException
    {
	try
	{
	    return newsDAO.getNews(newsId);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not get news.");
	    throw new ServiceException(e);
	}
    }

    /*public List<News> getAllNews() throws ServiceException
    {
	try
	{
	    return newsDAO.getAllNews();
	}
	catch (DAOException e)
	{
	    throw new ServiceException(e);
	}
    }*/

    public void updateNews(News news) throws ServiceException
    {
	try
	{
	    newsDAO.updateNews(news);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not update news.");
	    throw new ServiceException(e);
	}
    }

    public void deleteNews(Long newsId) throws ServiceException
    {
	try
	{
	    newsDAO.deleteNews(newsId);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not delete news.");
	    throw new ServiceException(e);
	}
    }

    public List<News> searchNews(SearchCriteria searchCriteria) throws ServiceException
    {
	try
	{
	    return newsDAO.searchNews(searchCriteria);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not search news.");
	    throw new ServiceException(e);
	}
    }

    public Integer countAllNews() throws ServiceException
    {
	try
	{
	    return newsDAO.countAllNews();
	}
	catch (DAOException e)
	{
	    LOG.error("Can not count news.");
	    throw new ServiceException(e);
	}
    }
}
