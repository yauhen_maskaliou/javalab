package com.epam.newsmanagment.dao.impl;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByType;

import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.entity.Author;

@DataSet("authordao/dataset.xml")
@SpringApplicationContext("test-context.xml")
public class AuthorDAOImplTest extends UnitilsJUnit4
{
    @SpringBeanByType
    private IAuthorDAO authorDAO;
    
    @Test
    @ExpectedDataSet("authordao/dataset-expected-add.xml")
    public void testAddNewsAuthorRelation() throws Exception
    {
	authorDAO.addNewsAuthorRelation(3L, 1L);
    }
    
    @Test
    @ExpectedDataSet("authordao/dataset-expected-expire.xml")
    public void testExpireAuthor() throws Exception
    {
	authorDAO.expireAuthor(2L, Timestamp.valueOf("2015-10-18 12:21:59"));
    }
    
    @Test
    public void testGetAuthor() throws Exception
    {
	Author expectedAuthor = new Author(1L, "test name 1", Timestamp.valueOf("2015-05-10 12:12:12"));
	Author author = authorDAO.getAuthor(1L);
	assertEquals(expectedAuthor.getId(), author.getId());
	assertEquals(expectedAuthor.getName(), author.getName());
	assertEquals(expectedAuthor.getExpired(), author.getExpired());
    }
    
    @Test
    public void testAddAuthor() throws Exception
    {
	Author expectedAuthor = new Author(0L, "test name 3", null);
	authorDAO.addAuthor(expectedAuthor);
	Author author = authorDAO.getAuthor(expectedAuthor.getId());
	assertEquals(expectedAuthor.getId(), author.getId());
	assertEquals(expectedAuthor.getName(), author.getName());
	assertEquals(expectedAuthor.getExpired(), author.getExpired());
    }
    
    @Test
    public void testUpdateAuthor() throws Exception
    {
	Author author = new Author(1L, "test name 11", Timestamp.valueOf("2015-05-10 12:12:12"));
	authorDAO.updateAuthor(author);
	assertEquals(authorDAO.getAuthor(1L).getName(), author.getName());
    }
}
