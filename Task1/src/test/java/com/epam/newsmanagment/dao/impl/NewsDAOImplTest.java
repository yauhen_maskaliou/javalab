package com.epam.newsmanagment.dao.impl;

import static org.junit.Assert.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByType;

import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;

@DataSet("newsdao/dataset.xml")
@SpringApplicationContext("test-context.xml")
public class NewsDAOImplTest extends UnitilsJUnit4
{
    @SpringBeanByType
    private INewsDAO newsDAO;

    @Test
    public void testGetNews() throws Exception
    {
	News expectedNews = new News(1L, "News title 1", "News short text 1", "News full text 1", 
		Timestamp.valueOf("2014-06-09 03:15:15"), Date.valueOf("2015-09-01"));
	News news = newsDAO.getNews(1L);
	assertEquals(expectedNews.getId(), news.getId());
	assertEquals(expectedNews.getTitle(), news.getTitle());
	assertEquals(expectedNews.getShortText(), news.getShortText());
	assertEquals(expectedNews.getFullText(), news.getFullText());
	assertEquals(expectedNews.getCreationDate(), news.getCreationDate());
	assertEquals(expectedNews.getModificationDate(), news.getModificationDate());
    }

    @Test
    public void testAddNews() throws Exception
    {
	News expectedNews = new News(0L, "Title", "TEXT", "text", Timestamp.valueOf("2015-06-06 11:11:11"),
		Date.valueOf("2015-10-07"));
	newsDAO.addNews(expectedNews);
	News news = newsDAO.getNews(expectedNews.getId());
	assertEquals(expectedNews.getId(), news.getId());
	assertEquals(expectedNews.getTitle(), news.getTitle());
	assertEquals(expectedNews.getShortText(), news.getShortText());
	assertEquals(expectedNews.getFullText(), news.getFullText());
	assertEquals(expectedNews.getCreationDate(), news.getCreationDate());
	assertEquals(expectedNews.getModificationDate(), news.getModificationDate());
    }

    @Test
    public void testUpdateNews() throws Exception
    {
	News news = new News(1L, "updated_title", "updated", "updated", Timestamp.valueOf("2015-07-07 12:12:12"),
		Date.valueOf("2015-10-11"));
	Date mod = newsDAO.getNews(1L).getModificationDate();
	newsDAO.updateNews(news);
	assertTrue(mod.before(newsDAO.getNews(1L).getModificationDate()));
    }

    @Test
    @DataSet("newsdao/dataset-delete.xml")
    @ExpectedDataSet("newsdao/dataset-expected-delete.xml")
    public void testDelete() throws Exception
    {
	newsDAO.deleteNews(2L);
    }

    @Test
    @DataSet("newsdao/dataset-search.xml")
    public void testSearch() throws Exception
    {
	List<Long> tags = new ArrayList<Long>();
	tags.add(2L);
	tags.add(3L);
	SearchCriteria sc = new SearchCriteria(1L, tags);
	List<News> list = newsDAO.searchNews(sc);
	assertEquals(3L, list.size());
	assertEquals(Long.valueOf(2L), list.get(0).getId());
	assertEquals(Long.valueOf(3L), list.get(1).getId());
	assertEquals(Long.valueOf(1L), list.get(2).getId());
    }
    
    @Test
    public void testCountAllNews() throws Exception
    {
	int number = newsDAO.countAllNews();
	assertEquals(2, number);
    }
}
