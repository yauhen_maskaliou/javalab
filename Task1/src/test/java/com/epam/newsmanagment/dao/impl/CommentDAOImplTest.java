package com.epam.newsmanagment.dao.impl;

import static org.junit.Assert.assertEquals;

import java.sql.Timestamp;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByType;

import com.epam.newsmanagment.dao.ICommentDAO;
import com.epam.newsmanagment.entity.Comment;

@DataSet("commentdao/dataset.xml")
@SpringApplicationContext("test-context.xml")
public class CommentDAOImplTest extends UnitilsJUnit4
{
    @SpringBeanByType
    private ICommentDAO commentDAO;

    @Test
    public void testGetComment() throws Exception
    {
	Comment expectedComment = new Comment(3L, 1L, "comment text 3", Timestamp.valueOf("2015-09-01 12:12:12"));
	Comment comment = commentDAO.getComment(3L);
	assertEquals(expectedComment.getId(), comment.getId());
	assertEquals(expectedComment.getNewsId(), comment.getNewsId());
	assertEquals(expectedComment.getText(), comment.getText());
	assertEquals(expectedComment.getCreationDate(), comment.getCreationDate());
    }

    @Test
    public void testAddComment() throws Exception
    {
	Comment expectedComment = new Comment(0L, 2L, "comment text 4", Timestamp.valueOf("2015-10-08 12:12:12"));
	commentDAO.addComment(expectedComment);
	Comment comment = commentDAO.getComment(expectedComment.getId());
	assertEquals(expectedComment.getId(), comment.getId());
	assertEquals(expectedComment.getNewsId(), comment.getNewsId());
	assertEquals(expectedComment.getText(), comment.getText());
	assertEquals(expectedComment.getCreationDate(), comment.getCreationDate());
    }

    @Test
    @ExpectedDataSet("commentdao/dataset-expected-delete.xml")
    public void testDeleteComment() throws Exception
    {
	commentDAO.deleteComment(2L);
    }
}
