package com.epam.newsmanagment.dao.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBeanByType;

import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.entity.Tag;

@DataSet("authordao/dataset.xml")
@SpringApplicationContext("test-context.xml")
public class TagDAOImplTest extends UnitilsJUnit4
{
    @SpringBeanByType
    private ITagDAO tagDAO;
    
    @Test
    @ExpectedDataSet("tagdao/dataset-expected-add.xml")
    public void testAddNewsTagRelations() throws Exception
    {
	List<Long> tagIds = new ArrayList<Long>();
	tagIds.add(2L);
	tagIds.add(4L);
	tagDAO.addNewsTagsRelations(2L, tagIds);
    }
    
    @Test
    public void testGetTag() throws Exception
    {
	Tag expectedTag = new Tag(1L, "sport");
	Tag tag= tagDAO.getTag(1L);
	assertEquals(expectedTag.getId(), tag.getId());
	assertEquals(expectedTag.getName(), tag.getName());
    }
    
    @Test
    public void testAddTag() throws Exception
    {
	Tag expectedTag = new Tag(1L, "football");
	tagDAO.addTag(expectedTag);
	Tag tag = tagDAO.getTag(expectedTag.getId());
	assertEquals(expectedTag.getId(), tag.getId());
	assertEquals(expectedTag.getName(), tag.getName());
    }
    
    @Test
    public void testUpdateTag() throws Exception
    {
	Tag tag = new Tag(1L, "sports");
	tagDAO.updateTag(tag);
	assertEquals(tagDAO.getTag(1L).getName(), tag.getName());
    }
}
