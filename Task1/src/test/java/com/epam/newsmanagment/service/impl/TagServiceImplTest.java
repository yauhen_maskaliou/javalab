package com.epam.newsmanagment.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.impl.TagServiceImpl;

public class TagServiceImplTest
{
    @Mock
    private ITagDAO tagDAO;
    
    private ITagService tagService;
    
    @Before
    public void init()
    {
	MockitoAnnotations.initMocks(this);
	tagService = new TagServiceImpl(tagDAO);
    }
    
    @Test
    public void testCountAllNews() throws Exception
    {
	List<Long> tags = new ArrayList<Long>();
	tagService.addNewsTagsRelations(0L, tags);
	verify(tagDAO, times(1)).addNewsTagsRelations(0L, tags);
    }
    
    @Test
    public void testGetTag() throws Exception
    {
	when(tagDAO.getTag(0L)).thenReturn(new Tag());
	
	tagService.getTag(0L);
	verify(tagDAO, times(1)).getTag(0L);
	assertEquals(tagService.getTag(0L), new Tag());
    }
    
    @Test
    public void testAddTag() throws Exception
    {
	tagService.addTag(new Tag());
	verify(tagDAO, times(1)).addTag(new Tag());
    }
    
    @Test
    public void testUpdateTag() throws Exception
    {
	tagService.updateTag(new Tag());
	verify(tagDAO, times(1)).updateTag(new Tag());
    }
}
