package com.epam.newsmanagment.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.impl.AuthorServiceImpl;

public class AuthorServiceImplTest
{
    @Mock
    private IAuthorDAO authorDAO;
    
    private IAuthorService authorService;
    
    @Before
    public void init()
    {
	MockitoAnnotations.initMocks(this);
	authorService = new AuthorServiceImpl(authorDAO);
    }
    
    @Test
    public void testAddNewsAuthorRelation() throws Exception
    {
	authorService.addNewsAuthorRelation(0L, 0L);
	verify(authorDAO, times(1)).addNewsAuthorRelation(0L, 0L);
    }
    
    @Test
    public void testExpireAuthor() throws Exception
    {
	authorService.expireAuthor(0L, new Timestamp(0L));
	verify(authorDAO, times(1)).expireAuthor(0L, new Timestamp(0L));
    }

    @Test
    public void testGetAuthor() throws Exception
    {
	when(authorDAO.getAuthor(0L)).thenReturn(new Author());
	
	authorService.getAuthor(0L);
	verify(authorDAO, times(1)).getAuthor(0L);
	assertEquals(authorService.getAuthor(0L), new Author());
    }
    
    @Test
    public void testAddAuthor() throws Exception
    {
	authorService.addAuthor(new Author());
	verify(authorDAO, times(1)).addAuthor(new Author());
    }
    
    @Test
    public void testUpdateAuthor() throws Exception
    {
	authorService.updateAuthor(new Author());
	verify(authorDAO, times(1)).updateAuthor(new Author());
    }
}
