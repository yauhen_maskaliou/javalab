package com.epam.newsmanagment.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagment.dao.ICommentDAO;
import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.service.ICommentService;
import com.epam.newsmanagment.service.impl.CommentServiceImpl;

public class CommentServiceImplTest
{
    @Mock
    private ICommentDAO commentDAO;
    
    private ICommentService commentService;
    
    @Before
    public void init()
    {
	MockitoAnnotations.initMocks(this);
	commentService = new CommentServiceImpl(commentDAO);
    }
    
    @Test
    public void testGetComment() throws Exception
    {
	when(commentDAO.getComment(0L)).thenReturn(new Comment());
	
	commentService.getComment(0L);
	verify(commentDAO, times(1)).getComment(0L);
	assertEquals(commentService.getComment(0L), new Comment());
    }
    
    @Test
    public void testAddComment() throws Exception
    {
	when(commentDAO.addComment(new Comment())).thenReturn(7L);
	
	commentService.addComment(new Comment());
	verify(commentDAO, times(1)).addComment(new Comment());
	assertEquals(commentService.addComment(new Comment()), (Long)7L);
    }
    
    @Test
    public void testDeleteComment() throws Exception
    {
	commentService.deleteComment(0L);
	verify(commentDAO, times(1)).deleteComment(0L);
    }
}
