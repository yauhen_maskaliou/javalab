package com.epam.newsmanagment.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.impl.NewsServiceImpl;

public class NewsServiceImplTest
{
    @Mock
    private INewsDAO newsDAO;
    @Mock
    private IAuthorDAO authorDAO;
    @Mock
    private ITagDAO tagDAO;

    private INewsService newsService;

    @Before
    public void init()
    {
	MockitoAnnotations.initMocks(this);
	newsService = new NewsServiceImpl(newsDAO, authorDAO, tagDAO);
    }

    @Test
    public void testAddNews() throws Exception
    {
	News news = new News();
	List<Long> tags = new ArrayList<Long>();
	Author author = new Author();
	
	newsService.addNews(news, author.getId(), tags);
	verify(newsDAO, times(1)).addNews(news);
	verify(authorDAO, times(1)).addNewsAuthorRelation(news.getId(), author.getId());
	verify(tagDAO, times(1)).addNewsTagsRelations(news.getId(), tags);	
    }

    @Test
    public void testUpdateNews() throws Exception
    {
	News news = new News();
	newsService.updateNews(news);
	verify(newsDAO, times(1)).updateNews(news);
    }

    @Test
    public void testDeleteNews() throws Exception
    {
	newsService.deleteNews(0L);
	verify(newsDAO, times(1)).deleteNews(0L);
    }

    @Test
    public void testGetNews() throws Exception
    {
	when(newsDAO.getNews(0L)).thenReturn(new News());
	
	newsService.getNews(0L);
	verify(newsDAO, times(1)).getNews(0L);
	assertEquals(newsService.getNews(0L), new News());
    }
    
    @Test
    public void testSearchNews() throws Exception
    {
	when(newsDAO.searchNews(new SearchCriteria())).thenReturn(new ArrayList<News>());
	
	newsService.searchNews(new SearchCriteria());
	verify(newsDAO, times(1)).searchNews(new SearchCriteria());
	assertEquals(newsService.searchNews(new SearchCriteria()), new ArrayList<News>());
    }
    
    @Test
    public void testCountAllNews() throws Exception
    {
	when(newsDAO.countAllNews()).thenReturn(6);
	
	newsService.countAllNews();
	verify(newsDAO, times(1)).countAllNews();
	assertEquals(newsService.countAllNews(), (Integer)6);
    }
}
