package com.epam.newsmanagment.command;

public enum CommandEnum
{
    EMPTY
    {
        {
            command = new EmptyCommand();
        }
    },
    
    
    SHOWNEWSLIST
    {
	{
	    command = new ShowNewsListCommand();
	}
    },
    
    SHOWNEWS
    {
	{
	    command = new ShowNewsCommand();
	}
    },
    
    POSTCOMMENT
    {
	{
	    command = new PostCommentCommand();
	}
    },
    
    FILTER
    {
	{
	    command = new FilterCommand();
	}
    };
    
    protected Command command;

    public Command getCommand()
    {
        return command;
    }
}
