package com.epam.newsmanagment.command;

import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.entity.NewsExtended;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.ICommentService;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.impl.CommentServiceImpl;
import com.epam.newsmanagment.service.impl.NewsServiceImpl;

public class PostCommentCommand implements Command
{    
    public String execute(HttpServletRequest request) throws ServiceException
    {
	INewsService newsService = (NewsServiceImpl) context.getBean("newsService");
	ICommentService commentService = (CommentServiceImpl) context.getBean("commentService");
	
	Long newsId = ((NewsExtended) request.getSession(true).getAttribute("news")).getId();
	commentService.addComment(new Comment((Long)0L, newsId, request.getParameter("commentText"), 
		new Timestamp(new java.util.Date().getTime())));
	
	NewsExtended news = newsService.getExtendedNews(newsId);

	request.getSession(true).setAttribute("news", news);

	return "/pages/news.jsp";
    }
}
