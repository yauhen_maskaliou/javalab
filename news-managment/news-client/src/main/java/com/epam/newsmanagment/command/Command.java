package com.epam.newsmanagment.command;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagment.exception.ServiceException;

public interface Command
{
    ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
    
    public String execute(HttpServletRequest request) throws ServiceException;
}
