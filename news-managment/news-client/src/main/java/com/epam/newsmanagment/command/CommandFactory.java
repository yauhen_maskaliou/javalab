package com.epam.newsmanagment.command;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagment.exception.ServiceException;

public class CommandFactory
{
    private static final String COMMAND_PARAMETER_NAME = "command";

    private CommandFactory()
    {
	
    }

    public static Command defineCommand(HttpServletRequest request) throws ServiceException
    {

        String command = request.getParameter(COMMAND_PARAMETER_NAME);
        if (command != null)
        {
            try
            {
                return CommandEnum.valueOf(command.toUpperCase()).getCommand();
            }
            catch (IllegalArgumentException e)
            {
                throw new ServiceException("Wrong command value.", e);
            }
        }
        else
        {
            //return CommandEnum.EMPTY.getCommand();
            return CommandEnum.SHOWNEWSLIST.getCommand();
        }
    }
}
