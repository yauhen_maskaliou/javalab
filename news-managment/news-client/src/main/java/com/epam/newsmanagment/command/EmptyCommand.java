package com.epam.newsmanagment.command;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagment.exception.ServiceException;

public class EmptyCommand implements Command
{
    public String execute(HttpServletRequest request) throws ServiceException
    {
	return "/index.jsp";
    }
}
