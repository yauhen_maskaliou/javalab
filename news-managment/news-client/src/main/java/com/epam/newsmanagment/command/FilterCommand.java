package com.epam.newsmanagment.command;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.impl.AuthorServiceImpl;
import com.epam.newsmanagment.service.impl.NewsServiceImpl;
import com.epam.newsmanagment.service.impl.TagServiceImpl;

public class FilterCommand implements Command
{
    public static final Long NEWSPERPAGE = 3L;
    
    @Override
    public String execute(HttpServletRequest request) throws ServiceException
    {
	INewsService newsService = (NewsServiceImpl) context.getBean("newsService");
	IAuthorService authorService = (AuthorServiceImpl) context.getBean("authorService");
	ITagService tagService = (TagServiceImpl) context.getBean("tagService");

	Long pageNumber = 1L;
	List<Long> list = new ArrayList<Long>();
	String[] tagIds = request.getParameterValues("tagIds");
	if(null != tagIds)
	{
	    for(String str : tagIds)
	    {
		list.add(Long.valueOf(str));
	    }
	}
	SearchCriteria searchCriteria = new SearchCriteria( Long.valueOf(request.getParameter("authorId")), list);
	Long first = NEWSPERPAGE * (pageNumber - 1) + 1;
	Long last = first + NEWSPERPAGE - 1;
	Long count = newsService.countSearchedNews(searchCriteria);
	
	request.getSession(true).
		setAttribute("pagesCount", count % NEWSPERPAGE == 0 ? count / NEWSPERPAGE : count / NEWSPERPAGE + 1);
	request.getSession(true).setAttribute("newsList", newsService.searchNews(searchCriteria));
	request.getSession(true).setAttribute("authorList", authorService.getAllAuthors());
	request.getSession(true).setAttribute("tagList", tagService.getAllTags());
	request.getSession(true).setAttribute("pageNumber", pageNumber);
	request.getSession(true).setAttribute("lang", "en");
	request.getSession(true).setAttribute("searchCriteria", searchCriteria);

	return "/pages/news-list.jsp";
    }

}
