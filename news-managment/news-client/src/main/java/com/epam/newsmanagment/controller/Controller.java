package com.epam.newsmanagment.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.newsmanagment.command.CommandFactory;
import com.epam.newsmanagment.exception.ServiceException;

@WebServlet("/act")
public class Controller extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    
    public static final Logger LOG = Logger.getRootLogger();

    public Controller()
    {
	
    }

    protected void doGet(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException
    {	
	processRequest(request, response);
    } 

    protected void doPost(HttpServletRequest request,
	    HttpServletResponse response) throws ServletException, IOException
    {
	processRequest(request, response);
    }
    
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            getServletContext().getRequestDispatcher(CommandFactory.defineCommand(request).execute(request)).forward(request, response);
        }
        catch (ServiceException e)
        {
            LOG.error(e);
            request.setAttribute("message", e.getMessage());
            getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
        }
    }
}