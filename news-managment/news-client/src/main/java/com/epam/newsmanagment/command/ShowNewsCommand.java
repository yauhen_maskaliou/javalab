package com.epam.newsmanagment.command;

import javax.servlet.http.HttpServletRequest;

import com.epam.newsmanagment.entity.NewsExtended;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.impl.NewsServiceImpl;

public class ShowNewsCommand implements Command
{    
    public String execute(HttpServletRequest request) throws ServiceException
    {
	INewsService newsService = (NewsServiceImpl) context.getBean("newsService");

	Long newsId = Long.valueOf(request.getParameter("newsId"));
	NewsExtended news = newsService.getExtendedNews(newsId);
	SearchCriteria searchCriteria = (SearchCriteria) request.getSession(true).getAttribute("searchCriteria");
	
	Long currentIndex = newsService.getIndexOfCurrentNews(searchCriteria, newsId);
	    
	request.getSession(true).setAttribute("previousId", newsService.getPreviousNewsId(searchCriteria, currentIndex));
	request.getSession(true).setAttribute("nextId", newsService.getNextNewsId(searchCriteria, currentIndex));
	request.getSession(true).setAttribute("news", news);

	return "/pages/news.jsp";
    }
}
