<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>News Portal</title>
</head>

<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="label" />

<body>
	<div class="panel panel-primary">
		<jsp:include page="header.jsp" />

		<div class="panel panel-default">
			<div class="panel-heading">
				<div style="text-align:left">
					<b> <c:out value="${news.title}" /> </b> (by <c:out value="${news.author.name}" />)
				</div>
				<div align="right">
					<c:out value="${news.modificationDate}" />
				</div>
			</div>
			
			<div class="panel-body">
				<c:out value="${news.fullText}" /> 	<br /> <br /> <br />
				
				Comments: <br /> <br />
				
				<c:forEach var="it" items="${news.commentList}">
					<c:out value="${it.creationDate}" /> <br />
					
					<span style="background-color: lightgray;"> <c:out value="${it.text}" /> <br /> <br /> </span>
				</c:forEach>
				
				<form>
					<input type="hidden" name="command" value="postcomment">
					<input type="text" maxlength=100 id="commentText"
						class="input-block-level" name="commentText"> <br /> <br />
					<button class="btn btn-large btn-primary" type="submit" formaction="act"> Post comment </button>
				</form>
			</div>			
		</div>
		
		<table style="width:100%;">
			<tr>
				<td align="left">
					<form>
					<c:if test="${0 != previousId}">
						<input type="hidden" name="command" value="shownews">
			
						<button type="submit" name="newsId"
							value="<c:out value="${previousId}" />" formaction="act"
							style="margin-left: 5em"> Previous </button>
					</form>
					</c:if>
				</td>
				<td align="right">
					<form>
					<c:if test="${0 != nextId}">
						<input type="hidden" name="command" value="shownews">
			
						<button type="submit" name="newsId"
							value="<c:out value="${nextId}" />" formaction="act"
							style="margin-left: 5em"> Next </button>
					</form>
					</c:if>
				</td>
			</tr>
		</table>
		<jsp:include page="footer.jsp" />
	</div>
</body>
</html>