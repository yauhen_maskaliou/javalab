<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel='stylesheet'
	href='webjars/bootstrap/3.2.0/css/bootstrap.min.css'>
<title>Insert title here</title>
</head>
<body>
	<form>
		<input type="hidden" name="command" value="shownewsList">

		<button type="submit" class="btn btn-large btn-link" name="newsId"
			value="${it.id}" formaction="act" style="margin-left: 5em">
			Go to news list.
		</button>
	</form>
</body>
</html>