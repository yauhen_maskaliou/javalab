<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<fmt:setLocale value="${sessionScope.lang}" />
<fmt:setBundle basename="label" />

<link href="<c:url value="/resources/css/simplePagination.css" />" rel="stylesheet">

<script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui.dropdownchecklist-1.4-min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.simplePagination.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/script.js" />"></script>

<c:url var="rootUrl" value="/news-list" />

<script type="text/javascript">
	$(document).ready(function()
	{
		$("#multi-select").dropdownchecklist(
		{
			width : 200,
			maxDropHeight : 100,
			emptyText : "<fmt:message key="newsList.clickToSelectTags" />"
		});
	});
</script>

<title>News Portal</title>
</head>
<body>
	<div>
		<jsp:include page="header.jsp" />
		<div>
			
			<br />
			<div align="center" >
			<form action="act">
				<input type="hidden" name="command" value="filter">
				<table> <tr>
				<td>
				<fmt:message key="newsList.author" />: 
				<select style="margin-right: 5em" value="${searchCriteria.authorId}" name="authorId">
					<option value="0"> <fmt:message key="newsList.any" /> </option>
					<c:forEach var="author" items="${authorList}">
						<option value="<c:out value="${author.id}" />"
							<c:if test="${author.id == searchCriteria.authorId}">
								selected
							</c:if>
						>
							<c:out value="${author.name}" />
						</option>
					</c:forEach>
				</select>
				</td>
				
				<td>
				<fmt:message key="newsList.tags" />: 
				
				<select id="multi-select" name="tagIds" style="margin-right: 5em" multiple="multiple">
					<c:forEach var="tag" items="${tagList}">
						<option value="<c:out value="${tag.id}" />"
						<c:forEach items="${searchCriteria.tagIds}" var="id">
							<c:if test="${tag.id == id}">
								selected
							</c:if>
						</c:forEach>
						> 
							<c:out value="${tag.name}" />
						</option>
					</c:forEach>
				</select>
				
				</td>
				
				<td>
					<input type="submit" value="<fmt:message key="newsList.filter" />">
				</td>
				
				<td>
					<a href="${rootUrl}/reset">
						<button type="button"> <fmt:message key="newsList.reset" /> </button>
					</a>
				</td>
				
				</tr> </table>
			</form>
		</div>
		<br />
		
		<div style="height:480;">
		<table id="content" border="1" style="border-collapse: collapse; width:100%">
			<tbody>
				<c:forEach var="news" items="${newsList}">
					<tr>
						<td>
							<div>
								<div>
									<div style="text-align: left">
										<b> <c:out value="${news.title}" /> </b> 
										(<fmt:message key="newsList.by" /> <c:out value="${news.author.name}" />)
									</div>
									<div align="right">
										<c:set var="format">
											<fmt:message key="pattern.date" />
										</c:set>
										<fmt:message key="newsList.lastUpdated" />:
									</div>
								</div>
								<br />
								<div>
									<div>
										<c:out value="${news.shortText}" />
									</div>
									<br />
									<div align="right">
										<font color="gray"> 
											<fmt:message key="newsList.tags" />:  
											<c:forEach var="tag" items="${news.tagList}">
												<c:out value="${tag.name}" />. 
											</c:forEach>
										</font>

										<c:set var="number" value="0" />
										<c:forEach items="${news.commentList}">
											<c:set var="number" value="${number + 1}" />
										</c:forEach>

										<font color="red" style="margin-left: 5em"> 
											<fmt:message key="newsList.comments" />: (<c:out value="${number}" />)
										</font>
										
										<form>
											<input type="hidden" name="command" value="shownews">
								
											<button type="submit" class="btn btn-large btn-link" name="newsId" 
												value="<c:out value="${news.id}" />" formaction="act" style="margin-left:5em">
			 									View
			 								</button>
		 								</form>
									</div>
								</div>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div id="pagination" class="pagination" align="center"> </div>
		<br />
		</div>
		<jsp:include page="footer.jsp" />
	</div>
</body>
</html>