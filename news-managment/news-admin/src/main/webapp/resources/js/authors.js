var prevName;
var prevIndex;
var prevId;

function prepareForEdit(index, id)
{
	disableEdit(prevIndex, prevId);
	prevName = $("input[name='authors[" + index + "].name']").val();
	prevIndex = index;
	prevId = id;
	$("input[name='authors[" + index + "].name']").removeAttr("disabled");

	$("#edit" + id).hide();

	$("#update" + id).show();
	$("#expire" + id).show();
	$("#cancel" + id).show();

	$("#cancel" + id).click(function()
	{
		disableEdit(index, id)
	});
}

function disableEdit(index, id)
{
	$("input[name='authors[" + prevIndex + "].name']").val(prevName);
	$("input[name='authors[" + prevIndex + "].name']").attr("disabled",
			"disabled");

	$("#edit" + id).show();

	$("#update" + id).hide();
	$("#expire" + id).hide();
	$("#cancel" + id).hide();
}