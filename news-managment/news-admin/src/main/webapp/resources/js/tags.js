var prevName;
var prevIndex;
var prevId;

function prepareForEdit(index, id) {
	disableEdit(prevIndex, prevId);
	prevName = $("input[name='tags[" + index + "].name']").val();
	prevIndex = index;
	prevId = id;
	$("input[name='tags[" + index + "].name']").removeAttr("disabled");

	$("#edit" + id).hide();

	$("#update" + id).show();
	$("#delete" + id).show();
	$("#cancel" + id).show();

	$("#cancel" + id).click(function() {
		disableEdit(index, id)
	});
}

function disableEdit(index, id) {
	$("input[name='tags[" + prevIndex + "].name']").val(prevName);
	$("input[name='tags[" + prevIndex + "].name']").attr("disabled",
			"disabled");

	$("#edit" + id).show();

	$("#update" + id).hide();
	$("#delete" + id).hide();
	$("#cancel" + id).hide();
}