<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<link href="<c:url value="/resources/css/simplePagination.css" />"
	rel="stylesheet">

<script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/tags.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.simplePagination.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/script-authors-tags.js" />"></script>

<div style="width: 1000; height: 550;">

<c:url value="/admin/tags" var="rootUrl" />

<div style="height:400;">
<table cellspacing="50" id="content">
	<tbody>
		<form:form method="post" action="${rootUrl}/update" modelAttribute="tags">
		<c:forEach items="${tags.tags}" var="tag" varStatus="status">
			<tr>
				<td><b> <spring:message code="add_update_tags.tags" />: </b></td>
				<td>
					<form:input path="tags[${status.index}].id" type="hidden" />
					<form:input required="required" pattern=".{3,30}"
						path="tags[${status.index}].name" style="width:400" type="text" disabled="true" />
					<br/>
				</td>
				<td>
					<a href="javascript:prepareForEdit(${status.index},${tag.id})" id="edit${tag.id}">
						<spring:message code="add_update_tags.edit" />
					</a>
					<button id="update${tag.id}" name="action" value="${status.index}"
						type="submit" class="link-button" style="display: none; " >
							<spring:message code="add_update_tags.update" /> 
						</button>
						
					<a href="<c:url value="/admin/tags/${tag.id}/delete" />" id="delete${tag.id}"  
						 class="link-button" style="display: none" >
						 <spring:message code="add_update_tags.delete" />
					</a>

					<a href="javascript:void(0);" id="cancel${tag.id}" style="display: none">
						<spring:message code="add_update_tags.cancel" />
					</a>
				</td>
			</tr>
		</c:forEach>
		</form:form>
	</tbody>
</table>
	</div>
	<div id="pagination" class="pagination" align="center" style="margin-left:50px"> </div>
	
	<table cellspacing="50">
		<tr>
			<form:form method="post" action="${rootUrl}/add" modelAttribute="newTag">
				<td><b><spring:message code="add_update_tags.addTag" />: </b></td>
				<td>
					<form:input path="id" type="hidden" value="1" />
					<form:input path="name" required="required" pattern=".{3,30}" title="${nameMessage}" style="width:400"/>
					<br/>
					<form:errors path="name" cssClass="error"/>
				</td>
				<td><form:button class="link-button"> <spring:message code="add_update_tags.save" /> </form:button></td>
			</form:form>
		</tr>
	</table>

</div>