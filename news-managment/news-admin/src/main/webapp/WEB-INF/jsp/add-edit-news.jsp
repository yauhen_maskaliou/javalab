<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui.dropdownchecklist-1.4-min.js" />"></script>

<script type="text/javascript">
	$(document).ready(function()
	{
		$("#multi-select").dropdownchecklist(
		{
			width : 400,
			maxDropHeight : 100,
			emptyText : "Click to select tags"
		});
	});
</script>

<c:if test="${operation=='add'}">
	<c:url var="rootUrl" value="/admin/news/add" />
</c:if>
<c:if test="${operation=='edit'}">
	<c:url var="rootUrl" value="/admin/news/${news.id}/edit" />
</c:if>

<div style="width: 1000; height: 550;">
	<div align="center">
		<form:form modelAttribute="news" method="post" action="${rootUrl}/save">
			<jsp:useBean id="today" class="java.util.Date" />
			<form:input type="hidden" path="modificationDate"  value="${today}"/>
			<table>
				<tr>
			 		<td> <spring:message code="add_edit.title" />: </td> 
			 		<td> <form:input path="title" maxlength="30" /> </td>
			    </tr>
			    <tr>
			 		<td> <spring:message code="add_edit.date" /> (YYYY-MM-DD): </td> 
			 		<td> <input name="creationDate" value="${creationDate}" 
			 		pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))"
			 		/> </td>
			    </tr>
			    <tr>
			 		<td> <spring:message code="add_edit.brief" />: </td> 
			 		<td> <form:textarea maxlength="100" rows="4" cols="80" path="shortText" /> </td>
			    </tr>
			    <tr>
			 		<td> <spring:message code="add_edit.content" />: </td> 
			 		<td> <form:textarea maxlength="1000" rows="17" cols="80" path="fullText" /> </td>
			    </tr>
			 </table>
			 <br />
			 <div>
			 	<table>
					<tr>
				 		<td> <spring:message code="add_edit.author" />: </td>
				 		<td> <spring:message code="add_edit.tags" />: </td>
				    </tr>
			    	<tr>
				 		<td align="center">
					 		<select required="required" name="authorId" id="author-select">
								<c:forEach items="${authorList}" var="author">
									<c:if test="${author.expired lt today && author.id != newsExtended.author.id}">
										<option value="0" disabled 
											<c:if test="${author.id == newsExtended.author.id}">
												selected
											</c:if>
										> ${author.name} </option>
									</c:if>
									<c:if test="${!(author.expired lt today && author.id != newsExtended.author.id)}">
										<option value="${author.id}" 
											<c:if test="${author.id == newsExtended.author.id}">
												selected
											</c:if>
										> ${author.name} </option>
									</c:if>
							    </c:forEach>
							</select>
						</td>
				 		<td>
				 			<select id="multi-select" name="tagIdList" multiple>
				 				<c:forEach items="${tagList}" var="tag">
									<option value="${tag.id}"
										<c:forEach items="${newsExtended.tagList}" var="tag2">
											<c:if test="${tag.id == tag2.id}">
												selected
											</c:if>
										</c:forEach>
									> ${tag.name} </option>
								</c:forEach>
							</select>
				 		</td>
			        </tr>
				    <tr>
						<td></td>
						<td align="right"><input type="submit" 
							value="<spring:message code="add_edit.save" />" style="width: 65px; height: 45px">
						</td>
					</tr>
			    </table>		
			 </div>
		</form:form>
	</div>
</div>