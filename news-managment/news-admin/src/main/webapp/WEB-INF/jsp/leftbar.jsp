<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="sidebar-content" style="background: #666; padding: 20px; padding-top: 1px; width:200">
	<div id="menu-list" style="padding: 5px; background: #fff;">
		<ul type="circle">
			<li <c:if test="${'news-list'==current}">style="font-weight: bold;"</c:if> > <a href="<c:url value="/admin/news-list" />"> 
				<spring:message code="leftbar.newsList" /> 
			</a> </li>
			
			<li <c:if test="${'addNews'==current}">style="font-weight: bold;"</c:if> > <a href="<c:url value="/admin/news/add" />">
				<spring:message code="leftbar.addNews" />
			</a> </li>
			
			<li <c:if test="${'addUpdateAuthors'==current}"> style="font-weight: bold;" </c:if> > <a href="<c:url value="/admin/authors/add-update-authors" />">
				<spring:message code="leftbar.addUpdateAuthors" /> 
			</a> </li>
			
			<li <c:if test="${'addUpdateTags'==current}"> style="font-weight: bold;" </c:if> > <a href="<c:url value="/admin/tags/add-update-tags" />">
				<spring:message code="leftbar.addUpdateTags" />
			</a> </li>
		</ul>
	</div>
</div>