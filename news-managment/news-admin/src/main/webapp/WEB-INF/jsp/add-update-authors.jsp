<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<link href="<c:url value="/resources/css/simplePagination.css" />"
	rel="stylesheet">

<script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/authors.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.simplePagination.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/script-authors-tags.js" />"></script>

<div style="width: 1000; height: 550;">

<c:url value="/admin/authors" var="rootUrl" />

<div style="height:400;">
<table cellspacing="50" id="content">
	<tbody>
	<jsp:useBean id="today" class="java.util.Date" />
		<form:form method="post" action="${rootUrl}/update" modelAttribute="authors">
		<c:forEach items="${authors.authors}" var="author" varStatus="status">
			<tr>
				<td><b> <spring:message code="add_update_authors.author" />:</b></td>
				<td>
					<form:input path="authors[${status.index}].id" type="hidden" />
					<c:if test="${not empty author.expired}">
						<form:input path="authors[${status.index}].expired" type="hidden" />
					</c:if>
					<form:input required="required" pattern=".{3,30}"
						path="authors[${status.index}].name" style="width:400" type="text" disabled="true" />
					<br/>
				</td>
				<td>
					<a href="javascript:prepareForEdit(${status.index},${author.id})" id="edit${author.id}">
						<spring:message code="add_update_authors.edit" />
					</a>
					<button id="update${author.id}" name="action" value="${status.index}"
						type="submit" class="link-button" style="display: none; " >
						<spring:message code="add_update_authors.update" /> 
					</button>
					<c:if test="${!(author.expired lt today)}">
						<a href="<c:url value="/admin/authors/${author.id}/expire" />" id="expire${author.id}"  
						 class="link-button" style="display: none" >
						 	<spring:message code="add_update_authors.expire" />
						 </a>
					</c:if>
					<c:if test="${author.expired lt today}">
						<i style="color:gray">
							(<spring:message code="add_update_authors.expired" />)
						</i>
					</c:if>
					<a href="javascript:void(0);" id="cancel${author.id}" style="display: none">
						<spring:message code="add_update_authors.cancel" />
					</a>
				</td>
			</tr>
		</c:forEach>
		</form:form>
	</tbody>
</table>
	</div>
	<div id="pagination" class="pagination" align="center" style="margin-left:50px"> </div>
	
	<table cellspacing="50">
		<tr>
			<form:form method="post" action="${rootUrl}/add" modelAttribute="newAuthor">
				<td><b><spring:message code="add_update_authors.addAuthor" />: </b></td>
				<td>
					<form:input path="id" type="hidden" value="1" />
					<form:input path="name" required="required" pattern=".{3,30}" title="${nameMessage}" style="width:400"/>
					<br/>
					<form:errors path="name" cssClass="error"/>
				</td>
				<td><form:button class="link-button"> <spring:message code="add_update_authors.save" /> </form:button></td>
			</form:form>
		</tr>
	</table>

</div>