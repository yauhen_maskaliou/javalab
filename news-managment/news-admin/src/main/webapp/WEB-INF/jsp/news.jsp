<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<link href="<c:url value="/resources/css/simplePagination.css" />" rel="stylesheet">

<script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.simplePagination.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/script.js" />"></script>
		
<c:url var="rootUrl" value="/admin/news/${news.id}/comments" />
<c:set var="format">
	<spring:message code="pattern.date" />
</c:set>

	<div style="width:1000; height:550;">
		<div>
			<div>
				<div style="text-align:left">
					 <b> <c:out value="${news.title}" /> </b> (by <c:out value="${news.author.name}" />)
				</div>
				<div align="right">
					<spring:message code="newsList.lastUpdated" />:
					<fmt:formatDate value="${news.modificationDate}" var="modificationDateString" 
						pattern="${format}" />
					<c:out value="${modificationDateString}" />
				</div>
				<br />
			</div>
			
			<div>
				<div style="height:150;">
					<c:out value="${news.fullText}" /> 	<br /> <br />
				</div>
				
				<spring:message code="news.comments" />: <br />
				
				<div style="height:150;">
				<table id="content" border="1"><tbody>
				<c:forEach var="comment" items="${news.commentList}">
					<tr><td>
					
					<fmt:formatDate value="${comment.creationDate}" var="commentDateString" 
						pattern="${format}" />
					<c:out value="${commentDateString}" /> <br />
					
					<span style="background-color: lightgray; width: 308px; height: 105px"> 
						<c:out value="${comment.text}" /> 
					</span>
					<span style="margin-left: 5px;">
						(<a href="<c:url value="/admin/news/${news.id}/comments/${comment.id}/delete" />"> 
							<spring:message code="news.delete" />
						</a>)
					</span>
					</td></tr>
				</c:forEach>
				</tbody> </table>
				</div>
				<div id="pagination" class="pagination" align="center"> </div> <br />
					<form:form action="${rootUrl}/post">
						<input type="text" maxlength=100 name="commentText"> <br />
						<input type="submit" value="<spring:message code="news.postComment" />" style="width: 158px; height: 45px" />
					</form:form>
			</div>			
		</div>
		<table style="width:100%;">
			<tr>
				<td align="left">
					<c:if test="${0 != previousId}">
						<a href="<c:url value="/admin/news/${previousId}/view" />"> 
							<spring:message code="news.previous" />
						</a>
					</c:if>
				</td>
				<td align="right">
					<c:if test="${0 != nextId}">
						<a href="<c:url value="/admin/news/${nextId}/view" />"> 
						<spring:message code="news.next" />
					</a>
					</c:if>
				</td>
			</tr>
		</table>
	</div>