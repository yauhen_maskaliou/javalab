<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security"%>

<table style="width:100%;">
	<tr>
		<td>
			<h1> <font color="blue"> <spring:message code="header.text" /> </font> </h1>
		</td>
		<td align="right">
			<label style="margin-right: 20px">
				<security:authorize url="/**">
					<c:url value="/logout" var="logoutUrl" />
					<form:form action="${logoutUrl}" method="post">
						<b> <spring:message code="header.hello" />, <security:authentication property="principal.username" /> </b>
						<input type="submit" value="<spring:message code="header.logout"/>" style="margin-left: 30px;" />
					</form:form>
				</security:authorize>
	 		</label>
	 		<br /> <br />
	 		<a href="?lang=en"> EN </a>&nbsp;
			<a href="?lang=ru"> РУ </a>
		</td>
	</tr>
</table>

