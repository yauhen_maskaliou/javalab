<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link href="<c:url value="/resources/css/simplePagination.css" />" rel="stylesheet">

<script type="text/javascript" src="<c:url value="/resources/js/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ui.dropdownchecklist-1.4-min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.simplePagination.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/script.js" />"></script>

<c:url var="homePageUrl" value="/admin/news-list" />

<script type="text/javascript">
	$(document).ready(function()
	{
		$("#multi-select").dropdownchecklist(
		{
			width : 200,
			maxDropHeight : 100,
			emptyText : "<spring:message code="newsList.clickToSelectTags" />"
		});
		
		var count = ${pagesCount};
		var current = ${pageNumber};
		var href = "${homePageUrl}";
		$("#pagination").pagination(
		{
			pages: count,
	        displayedPages: 15,
	        currentPage: current,
	        hrefTextPrefix : "${homePageUrl}/",
	        prevText : "<spring:message code="pagination.prev" />",
	        nextText : "<spring:message code="pagination.next" />"
	    });
	});
</script>

<c:url var="rootUrl" value="/admin/news-list" />

<div style="width:1000; height:600;">
	<div>
		<br />
		<div align="center" >
			<form:form modelAttribute="searchCriteria" action="${rootUrl}/filter" >
				<table> <tr>
				<td>
				<spring:message code="newsList.author" />: 
				<form:select style="margin-right: 5em" path="authorId" >
					<form:option value="0"> <spring:message code="newsList.any" /> </form:option>
					<c:forEach var="author" items="${authorList}">
						<form:option value="${author.id}">
							<c:out value="${author.name}" />
						</form:option>
					</c:forEach>
				</form:select>
				</td>
				
				<td>
				<spring:message code="newsList.tags" />: 
				
				<form:select path="tagIds" id="multi-select"  style="margin-right: 5em">
					<form:options items="${tagList}" itemValue="id" itemLabel="name"></form:options>
				</form:select> 
				</td>
				
				<td>
					<input type="submit" value="<spring:message code="newsList.filter" />">
				</td>
				
				<td>
					<a href="${rootUrl}/reset">
						<button type="button"> <spring:message code="newsList.reset" /> </button>
					</a>
				</td>
				
				</tr> </table>
			</form:form>
		</div>
		
		<form:form method="post" action="${rootUrl}/delete" modelAttribute="deleteList">
		<div style="height:480;">
		<table id="content" border="1" style="border-collapse: collapse; width:100%">
			<tbody>
				<c:forEach var="news" items="${newsList}">
					<tr>
						<td>
							<div>
								<div>
									<div style="text-align: left">
										<a href="<c:url value="/admin/news/${news.id}/view" />"> 
											<b> <c:out value="${news.title}" /> </b> 
										</a>
										(<spring:message code="newsList.by" /> <c:out value="${news.author.name}" />)
									</div>
									<div align="right">
										<c:set var="format">
											<spring:message code="pattern.date" />
										</c:set>
										<spring:message code="newsList.lastUpdated" />:
										<fmt:formatDate value="${news.modificationDate}" var="modificationDateString" 
										pattern="${format}" />
										<c:out value="${modificationDateString}" />
									</div>
								</div>
								<br />
								<div>
									<div>
										<c:out value="${news.shortText}" />
									</div>
									<br />
									<div align="right">
										<font color="gray"> 
											<spring:message code="newsList.tags" />:  
											<c:forEach var="tag" items="${news.tagList}">
												<c:out value="${tag.name}" />. 
											</c:forEach>
										</font>

										<c:set var="number" value="0" />
										<c:forEach items="${news.commentList}">
											<c:set var="number" value="${number + 1}" />
										</c:forEach>

										<font color="red" style="margin-left: 5em"> 
											<spring:message code="newsList.comments" />: (<c:out value="${number}" />)
										</font>

										<form:checkbox path="ids" value="${news.id}" style="margin-left: 5em"/>
										<a href="<c:url value="/admin/news/${news.id}/edit" />" style="margin-left: 2em"> 
											<spring:message code="newsList.edit" /> 
										</a>
									</div>
								</div>
							</div>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<br />
		
		<dialog id="dialog" style="width:50%;background-color:#F4FFEF;border:1px dotted black;">
			<p> <spring:message code="dialog.text1" /> </p>
			<input type="submit" value="<spring:message code="dialog.yes" />" id="yes" />
			<button type="button" id="no"> <spring:message code="dialog.no" /> </button>
		</dialog>

		<div align="right" style="margin-right: 3em">
			<button id="showDialog" type="button"> <spring:message code="newsList.delete" /> </button>
		</div>
		
		<script type="text/JavaScript">
			(function()
			{
			    var dialog = document.getElementById('dialog');  
			    document.getElementById('showDialog').onclick = function()
			    {  
			        dialog.show();  
			    };  
			    document.getElementById('yes').onclick = function()
			    {  
			        dialog.close();  
			    };
			    document.getElementById('no').onclick = function()
			    {  
			        dialog.close();  
			    };   
			})(); 
		</script>
		
		</div>
		</form:form>
		
		<div id="pagination" class="pagination" align="center"> </div>
	</div>
</div>
