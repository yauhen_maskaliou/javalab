package com.epam.newsmanagment.utils;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;

import com.epam.newsmanagment.entity.DeleteList;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ITagService;

public class NewsListUtils
{
    public static final Long NEWSPERPAGE = 3L;

    public static String prepareNewsListPage(int pageNumber, HttpSession session, INewsService newsService,
	    ITagService tagService, IAuthorService authorService, Model model) throws ServiceException
    {
	SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
	if (searchCriteria == null)
	{
	    searchCriteria = new SearchCriteria();
	    session.setAttribute("searchCriteria", searchCriteria);
	}

	Long first = NEWSPERPAGE * (pageNumber - 1) + 1;
	Long last = first + NEWSPERPAGE - 1;
	Long count = newsService.countSearchedNews(searchCriteria);

	model.addAttribute("pagesCount", count % NEWSPERPAGE == 0 ? count / NEWSPERPAGE : count / NEWSPERPAGE + 1);
	model.addAttribute("newsList", newsService.searchNews(searchCriteria, first, last));
	model.addAttribute("searchCriteria", searchCriteria);
	model.addAttribute("authorList", authorService.getAllAuthors());
	model.addAttribute("tagList", tagService.getAllTags());
	model.addAttribute("deleteList", new DeleteList());
	model.addAttribute("pageNumber", pageNumber);
	model.addAttribute("current", "news-list");

	return "news-list";
    }
}
