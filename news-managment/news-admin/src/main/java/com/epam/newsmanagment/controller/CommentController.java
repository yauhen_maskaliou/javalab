package com.epam.newsmanagment.controller;

import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.ICommentService;
import com.epam.newsmanagment.service.impl.CommentServiceImpl;

@Controller
@RequestMapping("/admin/news/{newsId}/comments")
public class CommentController
{
    private ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
    private ICommentService commentService;
    
    public static final Logger LOG = Logger.getRootLogger();
    
    @RequestMapping("/post")
    public String postComment(@PathVariable("newsId") Long newsId, 
	    @RequestParam("commentText") String commentText, Model model)
    {
	commentService = (CommentServiceImpl) context.getBean("commentService");

	try
	{
	    commentService.addComment(new Comment(0L, newsId, commentText, new Timestamp(System.currentTimeMillis())));
	}
	catch (ServiceException e)
	{
	    LOG.error(e);
	}

	return "redirect:/admin/news/" + newsId + "/view";
    }
    
    @RequestMapping("/{commentId}/delete")
    public String deleteComment(@PathVariable("newsId") Long newsId, 
	    @PathVariable("commentId") Long commentId, Model model)
    {
	commentService = (CommentServiceImpl) context.getBean("commentService");

	try
	{
	    commentService.deleteComment(commentId);
	}
	catch (ServiceException e)
	{
	    LOG.error(e);
	}

	return "redirect:/admin/news/" + newsId + "/view";
    }
}
