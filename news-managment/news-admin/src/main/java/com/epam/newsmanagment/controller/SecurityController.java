package com.epam.newsmanagment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SecurityController
{
    @RequestMapping("/logout")
    public String logout()
    {
	return "redirect:/login?logout";
    }
    
    @RequestMapping("/denied")
    public String accessDenied()
    {
	return "error";
    }
}
