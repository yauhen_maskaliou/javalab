package com.epam.newsmanagment.controller;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.entity.TagsListWrapper;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.impl.TagServiceImpl;

@Controller
@RequestMapping("/admin/tags")
public class TagController
{
    private ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
    private ITagService TagService;

    public static final Logger LOG = Logger.getRootLogger();

    @ModelAttribute("tags")
    public TagsListWrapper getTags() throws ServiceException
    {
	TagService = (TagServiceImpl) context.getBean("tagService");
	return new TagsListWrapper(TagService.getAllTags());
    }

    @RequestMapping("/add-update-tags")
    public String showAddUpdateTagsPage(Model model)
    {
	try
	{
	    TagService = (TagServiceImpl) context.getBean("tagService");

	    model.addAttribute("tags", new TagsListWrapper(TagService.getAllTags()));
	    model.addAttribute("newTag", new Tag());
	    model.addAttribute("current", "addUpdateTags");
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    return "error";
	}

	return "add-update-tags";
    }

    @RequestMapping("/{id}/delete")
    public String expireTag(@PathVariable("id") Long id, Model model)
    {
	try
	{
	    TagService = (TagServiceImpl) context.getBean("tagService");

	    TagService.deleteTag(id);
	    model.addAttribute("tags", new TagsListWrapper(TagService.getAllTags()));
	    model.addAttribute("newTag", new Tag());
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    return "error";
	}

	return "redirect:/admin/tags/add-update-tags";
    }

    @RequestMapping("/update")
    public String updateTag(@Valid @ModelAttribute("Tags") TagsListWrapper Tags, 
	    BindingResult bindingResult, @RequestParam("action") Integer actionParam, Model model)
    {
	try
	{
	     TagService = (TagServiceImpl) context.getBean("tagService");
	     
	     Tag Tag = Tags.get(actionParam);
	     TagService.updateTag(Tag); 
	     model.addAttribute("tags", new TagsListWrapper(TagService.getAllTags())); 
	     model.addAttribute("newTag", new Tag()); 
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    return "error";
	}

	return "redirect:/admin/tags/add-update-tags";
    }
    
    @RequestMapping("/add")
    public String addTag(@Valid @ModelAttribute("newTag") Tag Tag, 
	    BindingResult bindingResult, Model model)
    {
	try
	{
	     TagService = (TagServiceImpl) context.getBean("tagService");
	     
	     TagService.addTag(Tag);
	     model.addAttribute("tags", new TagsListWrapper(TagService.getAllTags())); 
	     model.addAttribute("newTag", new Tag()); 
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    return "error";
	}

	return "redirect:/admin/tags/add-update-tags";
    }
}
