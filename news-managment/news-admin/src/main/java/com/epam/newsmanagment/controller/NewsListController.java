package com.epam.newsmanagment.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.newsmanagment.entity.DeleteList;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.impl.AuthorServiceImpl;
import com.epam.newsmanagment.service.impl.NewsServiceImpl;
import com.epam.newsmanagment.service.impl.TagServiceImpl;
import com.epam.newsmanagment.utils.NewsListUtils;

@Controller
@RequestMapping("/admin/news-list")
public class NewsListController
{
    public static final Logger LOG = Logger.getRootLogger();
    
    private ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
    private INewsService newsService;
    private IAuthorService authorService;
    private ITagService tagService;
    
    @ModelAttribute("deleteList")
    public DeleteList deleteList()
    {
	return new DeleteList();
    }

    @RequestMapping("")
    public String showNewsList(Model model, HttpSession session)
    {
	newsService = (NewsServiceImpl) context.getBean("newsService");
	authorService = (AuthorServiceImpl) context.getBean("authorService");
	tagService = (TagServiceImpl) context.getBean("tagService");
	
	try
	{
	    return NewsListUtils.prepareNewsListPage(1, session, newsService, tagService, authorService, model);
	}
	catch (ServiceException e)
	{
	    LOG.error(e); 
	    return "error";
	}
    }
    
    @RequestMapping("/{pageNumber}")
    public String showNewsList(@PathVariable(value = "pageNumber") Integer pageNumber, Model model, HttpSession session)
    {
	newsService = (NewsServiceImpl) context.getBean("newsService");
	authorService = (AuthorServiceImpl) context.getBean("authorService");
	tagService = (TagServiceImpl) context.getBean("tagService");
	
	try
	{
	    return NewsListUtils.prepareNewsListPage(pageNumber, session, newsService, tagService, authorService, model);
	}
	catch (ServiceException e)
	{
	    LOG.error(e); 
	    return "error";
	}
    }
    
    @RequestMapping("/filter")
    public String showNewsListAccordingToSearchCriteria(@Valid @ModelAttribute("searchCriteria") SearchCriteria searchCriteria, 
	    Model model, HttpSession session)
    {
	session.setAttribute("searchCriteria", searchCriteria);

	return "redirect:/admin/news-list";
    }
    
    @RequestMapping("/delete")
    public String deleteNews(@Valid @ModelAttribute("deleteList") DeleteList deleteList, 
	    BindingResult bindingResult, Model model)
    {
	newsService = (NewsServiceImpl) context.getBean("newsService");
	authorService = (AuthorServiceImpl) context.getBean("authorService");
	tagService = (TagServiceImpl) context.getBean("tagService");
	
	try
	{
	    newsService.deleteNews(deleteList.getIds());
	}
	catch (ServiceException e)
	{
	    LOG.error(e); 
	    return "error";
	}
	
	LOG.info("News were deleted.");
	return "redirect:/admin/news-list";
    }
    
    @RequestMapping("/reset")
    public String reset(Model model, HttpSession session)
    {
	session.setAttribute("searchCriteria", new SearchCriteria());
	
	return "redirect:/admin/news-list";
    }
}
