package com.epam.newsmanagment.controller;

import java.sql.Timestamp;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.entity.AuthorsListWrapper;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.impl.AuthorServiceImpl;

@Controller
@RequestMapping("/admin/authors")
public class AuthorController
{
    interface Bable
    {
	
    }
    
    private ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
    private IAuthorService authorService;

    public static final Logger LOG = Logger.getRootLogger();

    @ModelAttribute("authors")
    public AuthorsListWrapper getAuthors() throws ServiceException
    {
	authorService = (AuthorServiceImpl) context.getBean("authorService");
	return new AuthorsListWrapper(authorService.getAllAuthors());
    }

    @RequestMapping("/add-update-authors")
    public String showAddUpdateAuthorsPage(Model model)
    {
	try
	{
	    authorService = (AuthorServiceImpl) context.getBean("authorService");

	    model.addAttribute("authors", new AuthorsListWrapper(authorService.getAllAuthors()));
	    model.addAttribute("newAuthor", new Author());
	    model.addAttribute("current", "addUpdateAuthors");
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    return "error";
	}

	return "add-update-authors";
    }

    @RequestMapping("/{id}/expire")
    public String expireAuthor(@PathVariable("id") Long id, Model model)
    {
	try
	{
	    authorService = (AuthorServiceImpl) context.getBean("authorService");

	    authorService.expireAuthor(id, new Timestamp(System.currentTimeMillis()));
	    model.addAttribute("authors", new AuthorsListWrapper(authorService.getAllAuthors()));
	    model.addAttribute("newAuthor", new Author());
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    return "error";
	}
	
	return "redirect:/admin/authors/add-update-authors";
    }

    @RequestMapping("/update")
    public String updateAuthor(@Valid @ModelAttribute("authors") AuthorsListWrapper authors, 
	    BindingResult bindingResult, @RequestParam("action") Integer actionParam, Model model)
    {
	try
	{
	     authorService = (AuthorServiceImpl) context.getBean("authorService");
	     
	     Author author = authors.get(actionParam);
	     authorService.updateAuthor(author); 
	     model.addAttribute("authors", new AuthorsListWrapper(authorService.getAllAuthors())); 
	     model.addAttribute("newAuthor", new Author()); 
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    return "error";
	}
	
	return "redirect:/admin/authors/add-update-authors";
    }
    
    @RequestMapping("/add")
    public String addAuthor(@Valid @ModelAttribute("newAuthor") Author author, 
	    BindingResult bindingResult, Model model)
    {
	try
	{
	     authorService = (AuthorServiceImpl) context.getBean("authorService");
	     
	     authorService.addAuthor(author);
	     model.addAttribute("authors", new AuthorsListWrapper(authorService.getAllAuthors())); 
	     model.addAttribute("newAuthor", new Author()); 
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    return "error";
	}

	return "redirect:/admin/authors/add-update-authors";
    }
}
