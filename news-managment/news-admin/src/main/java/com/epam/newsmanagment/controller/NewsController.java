package com.epam.newsmanagment.controller;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.NewsExtended;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.IAuthorService;
import com.epam.newsmanagment.service.INewsService;
import com.epam.newsmanagment.service.ITagService;
import com.epam.newsmanagment.service.impl.AuthorServiceImpl;
import com.epam.newsmanagment.service.impl.NewsServiceImpl;
import com.epam.newsmanagment.service.impl.TagServiceImpl;

@Controller
@RequestMapping("/admin/news")
public class NewsController
{
    private ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
    private INewsService newsService;
    private IAuthorService authorService;
    private ITagService tagService;

    public static final Logger LOG = Logger.getRootLogger();

    @RequestMapping("/{id}/view")
    public String showNews(@PathVariable("id") Long newsId, Model model, HttpSession session)
    {
	try
	{
	    newsService = (NewsServiceImpl) context.getBean("newsService");
	    SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute("searchCriteria");
	    
	    Long currentIndex = newsService.getIndexOfCurrentNews(searchCriteria, newsId);
	    
	    model.addAttribute("news", newsService.getExtendedNews(newsId));
	    model.addAttribute("previousId", newsService.getPreviousNewsId(searchCriteria, currentIndex));
	    model.addAttribute("nextId", newsService.getNextNewsId(searchCriteria, currentIndex));
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    return "error";
	}

	return "news";
    }

    @RequestMapping("/add")
    public String showAddNewsPage(Model model)
    {
	authorService = (AuthorServiceImpl) context.getBean("authorService");
	tagService = (TagServiceImpl) context.getBean("tagService");

	model.addAttribute("news", new News());
	model.addAttribute("newsExtended", new NewsExtended());
	model.addAttribute("operation", "add");
	model.addAttribute("current", "addNews");
	try
	{
	    model.addAttribute("authorList", authorService.getAllAuthors());
	    model.addAttribute("tagList", tagService.getAllTags());
	}
	catch (ServiceException e)
	{
	    LOG.error(e);
	    return "error";
	}

	return "add-edit-news";
    }
    
    @RequestMapping("/{id}/edit")
    public String showEditNewsPage(@PathVariable("id") Long id,  Model model)
    {
	newsService = (NewsServiceImpl) context.getBean("newsService");
	authorService = (AuthorServiceImpl) context.getBean("authorService");
	tagService = (TagServiceImpl) context.getBean("tagService");

	model.addAttribute("operation", "edit");
	try
	{
	    News news = newsService.getNews(id);
	    model.addAttribute("news", news);
	    model.addAttribute("newsExtended", newsService.getExtendedNews(id));
	    model.addAttribute("authorList", authorService.getAllAuthors());
	    model.addAttribute("tagList", tagService.getAllTags());
	    model.addAttribute("creationDate", new Date(news.getCreationDate().getTime()));
	}
	catch (ServiceException e)
	{
	    LOG.error(e);
	    return "error";
	}

	return "add-edit-news";
    }

    @RequestMapping(value = "/add/save", method = RequestMethod.POST)
    public String saveNews(@RequestParam("creationDate") String creationDate, @RequestParam("authorId") Long authorId, 
	    @RequestParam(value = "tagIdList", required = false) List<Long> tagIdList,
	    @Valid @ModelAttribute News news, BindingResult bindingResult, Model model)
    {
	try
	{
	    newsService = (NewsServiceImpl) context.getBean("newsService");
	    
	    java.util.Date date = Date.valueOf(creationDate);
	    news.setCreationDate(new Timestamp(date.getTime()));
	    news.setModificationDate(new Date(date.getTime()));
	    news.setId(0L); 
	    newsService.addNews(news, authorId, tagIdList);
	    
	    return "redirect:/admin/news/" + news.getId() + "/view";
	}
	catch (ServiceException e)
	{
	    LOG.error(e);
	    return "error";
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    return "error";
	}
    }
    
    @RequestMapping(value = "/{id}/edit/save", method = RequestMethod.POST)
    public String saveNewsAfterEditing(@PathVariable("id") Long id, @RequestParam("creationDate") String creationDate,
	    @RequestParam(value = "tagIdList", required = false) Long authorId, @RequestParam("tagIdList") List<Long> tagIdList,
	    @Valid @ModelAttribute("news") News news, BindingResult bindingResult, Model model)
    {
	try
	{
	    newsService = (NewsServiceImpl) context.getBean("newsService");
	    
	    java.util.Date date = Date.valueOf(creationDate);
	    news.setCreationDate(new Timestamp(date.getTime()));
	    news.setModificationDate(new Date(System.currentTimeMillis()));
	    news.setId(id);
	    newsService.updateNews(news, authorId, tagIdList);
	    
	    return "redirect:/admin/news/" + news.getId() + "/view";
	}
	catch (Exception e)
	{
	    LOG.error(e);
	    return "error";
	}
    }
}