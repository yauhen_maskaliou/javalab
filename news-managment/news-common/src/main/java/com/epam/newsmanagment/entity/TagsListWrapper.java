package com.epam.newsmanagment.entity;

import java.util.List;

import javax.validation.Valid;

public class TagsListWrapper
{
	@Valid
	private List<Tag> Tags;

	public TagsListWrapper()
	{

	}

	public TagsListWrapper(List<Tag> Tags)
	{
	    super();
	    this.Tags = Tags;
	}

	public List<Tag> getTags()
	{
	    return Tags;
	}

	public void setTags(List<Tag> Tags)
	{
	    this.Tags = Tags;
	}

	public Tag get(int index)
	{
	    return Tags.get(index);
	}
}
