package com.epam.newsmanagment.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.NestedCheckedException;

public class NewsExtended extends News implements Serializable
{
    private static final long serialVersionUID = 7608870682534577979L;
    
    private Author author;
    private List<Tag> tagList;
    private List<Comment> commentList;
    
    public NewsExtended(News news, Author author, List<Tag> tagList, List<Comment> commentList)
    {
	super(news.getId(), news.getTitle(), news.getShortText(), news.getFullText(), 
		news.getCreationDate(), news.getModificationDate());
	this.author = author;
	this.tagList = tagList;
	this.commentList = commentList;
    }

    public NewsExtended()
    {
	super();
    }

    public Author getAuthor()
    {
        return author;
    }

    public void setAuthor(Author author)
    {
        this.author = author;
    }

    public List<Tag> getTagList()
    {
        return tagList;
    }

    public void setTagList(List<Tag> tagList)
    {
        this.tagList = tagList;
    }

    public List<Comment> getCommentList()
    {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList)
    {
        this.commentList = commentList;
    }
    

    @Override
    public int hashCode()
    {
	final int prime = 31;
	int result = super.hashCode();
	result = prime * result + ((author == null) ? 0 : author.hashCode());
	result = prime * result + ((commentList == null) ? 0 : commentList.hashCode());
	result = prime * result + ((tagList == null) ? 0 : tagList.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj)
    {
	if (this == obj)
	    return true;
	if (!super.equals(obj))
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	NewsExtended other = (NewsExtended) obj;
	if (author == null)
	{
	    if (other.author != null)
		return false;
	}
	else if (!author.equals(other.author))
	    return false;
	if (commentList == null)
	{
	    if (other.commentList != null)
		return false;
	}
	else if (!commentList.equals(other.commentList))
	    return false;
	if (tagList == null)
	{
	    if (other.tagList != null)
		return false;
	}
	else if (!tagList.equals(other.tagList))
	    return false;
	return true;
    }

    @Override
    public String toString()
    {
	StringBuilder builder = new StringBuilder();
	builder.append("NewsExtended [");
	builder.append("news = ");
	builder.append(super.toString());
	builder.append(", author=");
	builder.append(author);
	builder.append(", tagList=");
	builder.append(tagList);
	builder.append(", commentList=");
	builder.append(commentList);
	builder.append("]");
	return builder.toString();
    }
}
