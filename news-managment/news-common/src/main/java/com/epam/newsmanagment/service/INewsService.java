package com.epam.newsmanagment.service;

import java.util.List;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.NewsExtended;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.ServiceException;

public interface INewsService
{
    public void addNews(News news, Long authorId, List<Long> tagIds) throws ServiceException;
    public News getNews(Long newsId) throws ServiceException;
    public void updateNews(News news, Long authorId, List<Long> tagIds) throws ServiceException;
    public void deleteNews(Long newsId) throws ServiceException;
    public List<NewsExtended> searchNews(SearchCriteria searchCriteria) throws ServiceException;
    public List<NewsExtended> searchNews(SearchCriteria searchCriteria, Long first, Long last) throws ServiceException;
    public Long countAllNews() throws ServiceException;
    public NewsExtended getExtendedNews(Long newsId) throws ServiceException;
    public Long countSearchedNews(SearchCriteria searchCriteria) throws ServiceException;
    public void deleteNews(List<Long> newsIds) throws ServiceException;
    public Long getIndexOfCurrentNews(SearchCriteria searchCriteria, Long newsId) throws ServiceException;
    public Long getPreviousNewsId(SearchCriteria searchCriteria, Long currentIndex) throws ServiceException;
    public Long getNextNewsId(SearchCriteria searchCriteria, Long currentIndex) throws ServiceException;
}
