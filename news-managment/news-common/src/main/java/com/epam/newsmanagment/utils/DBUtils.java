package com.epam.newsmanagment.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

public class DBUtils
{
    private static final Logger LOG = Logger.getLogger(DBUtils.class);

    public static Connection getConnection(DataSource ds) throws SQLException
    {
	return DataSourceUtils.getConnection(ds);
    }

    public static void closeResources(DataSource ds, Connection connection, ResultSet rs, Statement st)
    {
	if (rs != null)
	{
	    try
	    {
		rs.close();
	    }
	    catch (SQLException e)
	    {
		LOG.error("Unable to close Result Set.", e);
	    }
	}
	if (st != null)
	{
	    try
	    {
		st.close();
	    }
	    catch (SQLException e)
	    {
		LOG.error("Unable to close Statement.", e);
	    }
	}
	DataSourceUtils.releaseConnection(connection, ds);
    }
}
