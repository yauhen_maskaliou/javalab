package com.epam.newsmanagment.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.dao.ICommentDAO;
import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.NewsExtended;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.INewsService;

public class NewsServiceImpl implements INewsService
{
    private INewsDAO newsDAO;
    private IAuthorDAO authorDAO;
    private ITagDAO tagDAO;
    private ICommentDAO commentDAO;
    private final static Logger LOG = Logger.getLogger(NewsServiceImpl.class);

    public NewsServiceImpl(INewsDAO newsDAO, IAuthorDAO authorDAO, ITagDAO tagDAO, ICommentDAO commentDAO)
    {
	super();
	this.newsDAO = newsDAO;
	this.authorDAO = authorDAO;
	this.tagDAO = tagDAO;
	this.commentDAO = commentDAO;
    }

    @Override
    public void addNews(News news, Long authorId, List<Long> tagIds) throws ServiceException
    {
	try
	{
	    newsDAO.addNews(news);
	    authorDAO.addNewsAuthorRelation(news.getId(), authorId);
	    if(null != tagIds)
	    {
		tagDAO.addNewsTagsRelations(news.getId(), tagIds);
	    }
	}
	catch (DAOException e)
	{
	    LOG.error("Can not add news.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public News getNews(Long newsId) throws ServiceException
    {
	try
	{
	    return newsDAO.getNews(newsId);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not get news.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public void updateNews(News news, Long authorId, List<Long> tagIds) throws ServiceException
    {
	try
	{
	    newsDAO.updateNews(news);
	    authorDAO.updateNewsAuthorRelation(news.getId(), authorId);
	    tagDAO.deleteNewsTagsRelationsByNewsId(news.getId());
	    tagDAO.addNewsTagsRelations(news.getId(), tagIds);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not update news.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public void deleteNews(Long newsId) throws ServiceException
    {
	try
	{
	    newsDAO.deleteNews(newsId);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not delete news.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public List<NewsExtended> searchNews(SearchCriteria searchCriteria) throws ServiceException
    {
	try
	{
	    List<NewsExtended> list = new ArrayList<NewsExtended>();

	    for (Long l : newsDAO.searchNews(searchCriteria, 1L, newsDAO.countAllNews()))
	    {
		list.add(new NewsExtended(newsDAO.getNews(l), authorDAO.getAuthorByNewsId(l),
			tagDAO.getTagsByNewsIds(l), commentDAO.getCommentsByNewsId(l)));
	    }
	    return list;
	}
	catch (DAOException e)
	{
	    LOG.error(e);
	    throw new ServiceException("Can not search news.", e);
	}
    }

    @Override
    public Long countAllNews() throws ServiceException
    {
	try
	{
	    return newsDAO.countAllNews();
	}
	catch (DAOException e)
	{
	    LOG.error("Can not count news.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public NewsExtended getExtendedNews(Long newsId) throws ServiceException
    {
	try
	{
	    return new NewsExtended(newsDAO.getNews(newsId), authorDAO.getAuthorByNewsId(newsId),
		    tagDAO.getTagsByNewsIds(newsId), commentDAO.getCommentsByNewsId(newsId));
	}
	catch (DAOException e)
	{
	    LOG.error("Can not select extended news");
	    throw new ServiceException(e);
	}
    }

    @Override
    public List<NewsExtended> searchNews(SearchCriteria searchCriteria, Long first, Long last) throws ServiceException
    {
	try
	{
	    List<NewsExtended> list = new ArrayList<NewsExtended>();

	    for (Long l : newsDAO.searchNews(searchCriteria, first, last))
	    {
		list.add(new NewsExtended(newsDAO.getNews(l), authorDAO.getAuthorByNewsId(l),
			tagDAO.getTagsByNewsIds(l), commentDAO.getCommentsByNewsId(l)));
	    }
	    return list;
	}
	catch (DAOException e)
	{
	    LOG.error(e);
	    throw new ServiceException("Can not search news.", e);
	}
    }

    @Override
    public Long countSearchedNews(SearchCriteria searchCriteria) throws ServiceException
    {
	try
	{
	    return newsDAO.countSearchedNews(searchCriteria);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not count searched news.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public void deleteNews(List<Long> newsIds) throws ServiceException
    {
	try
	{
	    for (Long newsId : newsIds)
	    {
		newsDAO.deleteNews(newsId);
	    }
	}
	catch (DAOException e)
	{
	    LOG.error("Can not count searched news.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public Long getIndexOfCurrentNews(SearchCriteria searchCriteria, Long newsId) throws ServiceException
    {
	try
	{
	    return newsDAO.getIndexById(searchCriteria, newsId);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not get index of current news.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public Long getPreviousNewsId(SearchCriteria searchCriteria, Long currentIndex) throws ServiceException
    {
	try
	{    
	    if(currentIndex > 1)
	    {
		return newsDAO.searchNews(searchCriteria, currentIndex - 1, currentIndex).get(0);
	    }
	    else
	    {
		return 0L;
	    }
	}
	catch (DAOException e)
	{
	    LOG.error("Can not get previous news id.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public Long getNextNewsId(SearchCriteria searchCriteria, Long currentIndex) throws ServiceException
    {
	try
	{
	    if(currentIndex < newsDAO.countSearchedNews(searchCriteria))
	    {
		return newsDAO.searchNews(searchCriteria, currentIndex + 1, currentIndex + 2).get(0);
	    }
	    else
	    {
		return 0L;
	    }
	}
	catch (DAOException e)
	{
	    LOG.error("Can not get next news id.");
	    throw new ServiceException(e);
	}
    }
}
