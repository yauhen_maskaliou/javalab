package com.epam.newsmanagment.dao;

import java.util.List;

import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.exception.DAOException;

public interface ITagDAO
{
    public void addNewsTagsRelations(Long newsId, List<Long> tagIdList) throws DAOException;
    public Long addTag(Tag tag) throws DAOException;
    public void updateTag(Tag tag) throws DAOException;
    public Tag getTag(Long tagId) throws DAOException;
    public List<Tag> getTagsByNewsIds(Long newsId) throws DAOException;
    public List<Tag> getAllTags() throws DAOException;
    public void deleteNewsTagsRelationsByNewsId(Long newsId) throws DAOException;
    public void deleteNewsTagsRelationsByTagId(Long tagId) throws DAOException;
    public void deleteTag(Long tagId) throws DAOException;
}
