package com.epam.newsmanagment.dao;

import java.util.List;

import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.exception.DAOException;

public interface ICommentDAO
{
    public Long addComment(Comment comment) throws DAOException;
    public void deleteComment(Long commentId) throws DAOException;
    public Comment getComment(Long commentId) throws DAOException;
    public List<Comment> getCommentsByNewsId(Long newsId) throws DAOException;
}
