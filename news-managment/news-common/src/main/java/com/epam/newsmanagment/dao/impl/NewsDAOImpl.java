package com.epam.newsmanagment.dao.impl;

import static com.epam.newsmanagment.utils.DBUtils.closeResources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.epam.newsmanagment.dao.INewsDAO;
import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.utils.DBUtils;
import com.epam.newsmanagment.utils.SearchCriteriaUtils;

public class NewsDAOImpl implements INewsDAO
{
    private final static Logger LOG = Logger.getLogger(NewsDAOImpl.class);

    private static final String SQL_ADD_NEWS = "INSERT INTO news "
	    + "(news_id, title, short_text, full_text, creation_date, modification_date) "
	    + "VALUES (news_seq.nextval, ?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_NEWS = "UPDATE news "
	    + "SET title=?, short_text=?, full_text=?, creation_date=?, modification_date=? WHERE news_id=?";
    private static final String SQL_DELETE_NEWS = "DELETE FROM news WHERE news_id=?";
    private static final String SQL_GET_NEWS = "SELECT * FROM news WHERE news_id=?";
    private static final String SQL_COUNT_ALL_NEWS = "SELECT COUNT(*) FROM news";
    private static final String SQL_PAGINATION_START = "select "
	    + "NEWS_ID, TITLE, SHORT_TEXT,FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, COUNTED, RN "
	    + "from (select NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE, COUNTED, rownum RN "
	    + "from (select SEARCHED.NEWS_ID, SEARCHED.TITLE, SEARCHED.SHORT_TEXT, "
	    + "SEARCHED.FULL_TEXT, SEARCHED.CREATION_DATE, SEARCHED.MODIFICATION_DATE, SEARCHED.COUNTED, rownum RN from (";
    private static final String SQL_PAGINATION_END = ") SEARCHED order by "
	    + "SEARCHED.COUNTED desc nulls last, SEARCHED.MODIFICATION_DATE desc, SEARCHED.NEWS_ID asc) "
	    + "where rownum <= ? ) where rn >= ?";
    private static final String SQL_SEARCH_NEWS = "select "
	    + "N.NEWS_ID, N.TITLE, N.SHORT_TEXT, N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE, NEWS_COMMENTS.COUNTED "
	    + "from NEWS N left join (select count(C.COMMENT_ID) as COUNTED, C.NEWS_ID from COMMENTS C "
	    + "inner join NEWS N on C.NEWS_ID = N.NEWS_ID group by C.NEWS_ID) NEWS_COMMENTS on N.NEWS_ID = NEWS_COMMENTS.NEWS_ID ";
    private static final String SQL_COUNT_START = "select count(*) COUNT from (";
    private static final String SQL_COUNT_END = ")";
    private static final String SQL_GET_INDEX_BY_ID_START = "select RN from ("
	    + "select SEARCHED.NEWS_ID, SEARCHED.TITLE, SEARCHED.SHORT_TEXT, SEARCHED.FULL_TEXT, SEARCHED.CREATION_DATE, "
	    + "SEARCHED.MODIFICATION_DATE, SEARCHED.COUNTED, row_number() "
	    + "over (order by SEARCHED.COUNTED desc nulls last, SEARCHED.MODIFICATION_DATE desc, SEARCHED.NEWS_ID asc) RN from (";
    private static final String SQL_GET_INDEX_BY_ID_END = ") SEARCHED) FILTERED where NEWS_ID = ?";

    private DataSource dataSource;

    public NewsDAOImpl(DataSource dataSource)
    {
	super();
	this.dataSource = dataSource;
    }

    /**
     * Adds news to database.
     *
     * @param news
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public Long addNews(News news) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	Long returnId;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(SQL_ADD_NEWS, new String[] { "NEWS_ID" });
	    ps.setString(1, news.getTitle());
	    ps.setString(2, news.getShortText());
	    ps.setString(3, news.getFullText());
	    ps.setTimestamp(4, news.getCreationDate());
	    ps.setDate(5, news.getModificationDate());
	    ps.executeUpdate();

	    rs = ps.getGeneratedKeys();
	    if (!rs.next())
	    {
		throw new DAOException("Can't generate id for new news entity.");
	    }
	    returnId = rs.getLong(1);
	    news.setId(returnId);
	    return returnId;
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in news adding.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    /**
     * Updates news in database
     * 
     * @param news
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public void updateNews(News news) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(SQL_UPDATE_NEWS);
	    ps.setString(1, news.getTitle());
	    ps.setString(2, news.getShortText());
	    ps.setString(3, news.getFullText());
	    ps.setTimestamp(4, news.getCreationDate());
	    ps.setDate(5, news.getModificationDate());
	    ps.setLong(6, news.getId());
	    ps.executeUpdate();
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in news editing.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    /**
     * Deletes news from database.
     * 
     * @param news
     *            id
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public void deleteNews(Long newsId) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(SQL_DELETE_NEWS);
	    ps.setLong(1, newsId);
	    ps.executeUpdate();
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in news deletion.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    /**
     * Returns news by id.
     *
     * @param id
     * @return news.
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public News getNews(Long newsId) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(SQL_GET_NEWS);
	    ps.setLong(1, newsId);
	    rs = ps.executeQuery();
	    rs.next();
	    return new News(rs.getLong("news_id"), rs.getString("title"), rs.getString("short_text"),
		    rs.getString("full_text"), rs.getTimestamp("creation_date"), rs.getDate("modification_date"));
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in getting news.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    /**
     * Counts all news.
     * 
     * @return number of all news.
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public Long countAllNews() throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(SQL_COUNT_ALL_NEWS);
	    rs = ps.executeQuery();
	    rs.next();
	    return rs.getLong(1);
	}
	catch (SQLException e)
	{
	    System.out.println(e);
	    throw new DAOException("Error in counting news.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    /**
     * Returns list of news suiting to search criteria.
     *
     * @return list of news suiting to search criteria.
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public List<Long> searchNews(SearchCriteria searchCriteria, Long first, Long last) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    List<Long> list = new ArrayList<Long>();
	    connection = DBUtils.getConnection(dataSource);
	    if (searchCriteria == null)
	    {
		searchCriteria = new SearchCriteria();
	    }

	    StringBuffer sb = new StringBuffer(SQL_PAGINATION_START);
	    sb.append(SQL_SEARCH_NEWS);
	    sb.append(SearchCriteriaUtils.buildAuthorCriteria(searchCriteria));
	    sb.append(SearchCriteriaUtils.buildTagsCriteria(searchCriteria));
	    sb.append(SQL_PAGINATION_END);
	    ps = connection.prepareStatement(sb.toString());

	    SearchCriteriaUtils.setSearchValues(ps, searchCriteria, first, last);

	    rs = ps.executeQuery();
	    while (rs.next())
	    {
		list.add(rs.getLong("news_id"));
	    }
	    return list;
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in getting news.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    @Override
    public Long countSearchedNews(SearchCriteria searchCriteria) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);

	    StringBuffer sb = new StringBuffer(SQL_COUNT_START);
	    sb.append(SQL_SEARCH_NEWS);
	    if (searchCriteria == null)
	    {
		searchCriteria = new SearchCriteria();
	    }
	    sb.append(SearchCriteriaUtils.buildAuthorCriteria(searchCriteria));
	    sb.append(SearchCriteriaUtils.buildTagsCriteria(searchCriteria));
	    sb.append(SQL_COUNT_END);
	    ps = connection.prepareStatement(sb.toString());

	    SearchCriteriaUtils.setCountValues(ps, searchCriteria);

	    rs = ps.executeQuery();
	    rs.next();
	    return rs.getLong(1);
	}
	catch (SQLException e)
	{
	    LOG.error(e);
	    throw new DAOException("Error in counting searched news.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    @Override
    public Long getIndexById(SearchCriteria searchCriteria, Long newsId) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	if (searchCriteria == null)
	{
	    searchCriteria = new SearchCriteria();
	}
	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    
	    StringBuilder sb = new StringBuilder(SQL_GET_INDEX_BY_ID_START);
	    sb.append(SQL_SEARCH_NEWS);
	    sb.append(SearchCriteriaUtils.buildAuthorCriteria(searchCriteria));
	    sb.append(SearchCriteriaUtils.buildTagsCriteria(searchCriteria));
	    sb.append(SQL_GET_INDEX_BY_ID_END);
		
	    ps = connection.prepareStatement(sb.toString());
	    SearchCriteriaUtils.setIndexValues(ps, searchCriteria, newsId);
	    
	    rs = ps.executeQuery();
	    if (!rs.next())
	    {
		return 0L;
	    }
	    return rs.getLong("RN");
	}
	catch (SQLException e)
	{
	    throw new DAOException(e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }
}
