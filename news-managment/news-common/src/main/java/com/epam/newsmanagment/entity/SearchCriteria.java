package com.epam.newsmanagment.entity;

import java.util.List;

public class SearchCriteria
{
    private Long authorId;
    private List<Long> tagIds;

    public SearchCriteria()
    {
	super();
    }

    public SearchCriteria(Long authorId, List<Long> tagIds)
    {
	super();
	this.authorId = authorId;
	this.tagIds = tagIds;
    }

    public Long getAuthorId()
    {
	return authorId;
    }

    public void setAuthorId(Long authorId)
    {
	this.authorId = authorId;
    }

    public List<Long> getTagIds()
    {
	return tagIds;
    }

    public void setTagIds(List<Long> tagIds)
    {
	this.tagIds = tagIds;
    }

    @Override
    public int hashCode()
    {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((authorId == null) ? 0 : authorId.hashCode());
	result = prime * result + ((tagIds == null) ? 0 : tagIds.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj)
    {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	SearchCriteria other = (SearchCriteria) obj;
	if (authorId == null)
	{
	    if (other.authorId != null)
		return false;
	}
	else if (!authorId.equals(other.authorId))
	    return false;
	if (tagIds == null)
	{
	    if (other.tagIds != null)
		return false;
	}
	else if (!tagIds.equals(other.tagIds))
	    return false;
	return true;
    }

    @Override
    public String toString()
    {
	StringBuilder builder = new StringBuilder();
	builder.append("SearchCriteria [authorId=");
	builder.append(authorId);
	builder.append(", tagIds=");
	builder.append(tagIds);
	builder.append("]");
	return builder.toString();
    }
}
