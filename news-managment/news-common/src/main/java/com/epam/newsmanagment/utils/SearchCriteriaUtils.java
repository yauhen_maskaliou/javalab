package com.epam.newsmanagment.utils;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.epam.newsmanagment.entity.SearchCriteria;

public class SearchCriteriaUtils
{
    private static final Long ID0 = 0L;

    public static String buildAuthorCriteria(SearchCriteria searchCriteria)
    {
	if (searchCriteria.getAuthorId() != null && !ID0.equals(searchCriteria.getAuthorId()))
	{
	    return "inner join NEWS_AUTHORS NA on NA.NEWS_ID = N.NEWS_ID and NA.AUTHOR_ID = ? ";
	}
	return "";
    }

    public static String buildTagsCriteria(SearchCriteria searchCriteria)
    {
	StringBuilder sb = new StringBuilder();
	int tagsCount = 0;
	if (searchCriteria.getTagIds() != null)
	{
	    tagsCount = searchCriteria.getTagIds().size();
	}
	if (tagsCount != 0)
	{
	    sb.append("inner join NEWS_TAGS NT on NT.NEWS_ID = N.NEWS_ID and (NT.TAG_ID = ?");
	    for (int i = 0; i < tagsCount - 1; i++)
	    {
		sb.append(" or NT.TAG_ID = ?");
	    }
	    sb.append(") group by N.NEWS_ID, N.TITLE, "
		    + "N.SHORT_TEXT, N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE, NEWS_COMMENTS.COUNTED having count(*) = ? "
		    + "order by NEWS_COMMENTS.COUNTED desc nulls last, N.MODIFICATION_DATE desc, N.NEWS_ID asc ");
	    return sb.toString();
	}
	return "";
    }

    public static void setSearchValues(PreparedStatement ps, SearchCriteria searchCriteria, Long first, Long last)
	    throws SQLException
    {
	int tagsCount = 0;
	if (searchCriteria.getTagIds() != null)
	{
	    tagsCount = searchCriteria.getTagIds().size();
	}
	int shift = 0;
	if (searchCriteria.getAuthorId() != null && !ID0.equals(searchCriteria.getAuthorId()))
	{
	    shift++;
	    ps.setLong(shift, searchCriteria.getAuthorId());
	}
	if (tagsCount != 0)
	{
	    for (int i = 1; i <= tagsCount; i++)
	    {
		ps.setLong(i + shift, searchCriteria.getTagIds().get(i - 1));
	    }
	    ps.setInt(tagsCount + shift + 1, tagsCount);
	    shift++;
	}

	ps.setLong(tagsCount + shift + 1, last);
	ps.setLong(tagsCount + shift + 2, first);
    }

    public static void setCountValues(PreparedStatement ps, SearchCriteria searchCriteria) throws SQLException
    {
	int tagsCount = 0;
	if (searchCriteria.getTagIds() != null)
	{
	    tagsCount = searchCriteria.getTagIds().size();
	}
	int shift = 0;
	if (searchCriteria.getAuthorId() != null && !ID0.equals(searchCriteria.getAuthorId()))
	{
	    shift++;
	    ps.setLong(shift, searchCriteria.getAuthorId());
	}
	if (tagsCount != 0)
	{
	    for (int i = 1; i <= tagsCount; i++)
	    {
		ps.setLong(i + shift, searchCriteria.getTagIds().get(i - 1));
	    }
	    ps.setInt(tagsCount + shift + 1, tagsCount);
	    shift++;
	}
    }

    public static void setIndexValues(PreparedStatement ps, SearchCriteria searchCriteria, Long newsId) throws SQLException
    {
	int tagsCount = 0;
	if (searchCriteria.getTagIds() != null)
	{
	    tagsCount = searchCriteria.getTagIds().size();
	}
	int shift = 0;
	if (searchCriteria.getAuthorId() != null && !ID0.equals(searchCriteria.getAuthorId()))
	{
	    shift++;
	    ps.setLong(shift, searchCriteria.getAuthorId());
	}
	if (tagsCount != 0)
	{
	    for (int i = 1; i <= tagsCount; i++)
	    {
		ps.setLong(i + shift, searchCriteria.getTagIds().get(i - 1));
	    }
	    ps.setLong(tagsCount + shift + 1, tagsCount);
	    shift++;
	}
	ps.setLong(tagsCount + shift + 1, newsId);
    }
}
