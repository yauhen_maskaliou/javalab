package com.epam.newsmanagment.service;

import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.exception.ServiceException;

public interface ICommentService
{
    public Long addComment(Comment comment) throws ServiceException;
    public void deleteComment(Long commentId) throws ServiceException;
    public Comment getComment(Long commentId) throws ServiceException;
}
