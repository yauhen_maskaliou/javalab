package com.epam.newsmanagment.dao;

import java.util.List;

import com.epam.newsmanagment.entity.News;
import com.epam.newsmanagment.entity.SearchCriteria;
import com.epam.newsmanagment.exception.DAOException;

public interface INewsDAO
{
    public Long addNews(News news) throws DAOException;
    public News getNews(Long newsId) throws DAOException;
    public void updateNews(News news) throws DAOException;
    public void deleteNews(Long newsId) throws DAOException;
    public List<Long> searchNews(SearchCriteria searchCriteria, Long first, Long last) throws DAOException;
    public Long countSearchedNews(SearchCriteria searchCriteria) throws DAOException;
    public Long countAllNews() throws DAOException;
    public Long getIndexById(SearchCriteria searchCriteria, Long newsId) throws DAOException;
}
