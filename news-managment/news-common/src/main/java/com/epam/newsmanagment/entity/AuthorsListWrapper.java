package com.epam.newsmanagment.entity;

import java.util.List;

import javax.validation.Valid;

public class AuthorsListWrapper
{
	@Valid
	private List<Author> authors;

	public AuthorsListWrapper()
	{

	}

	public AuthorsListWrapper(List<Author> authors)
	{
	    super();
	    this.authors = authors;
	}

	public List<Author> getAuthors()
	{
	    return authors;
	}

	public void setAuthors(List<Author> authors)
	{
	    this.authors = authors;
	}

	public Author get(int index)
	{
	    return authors.get(index);
	}
}