package com.epam.newsmanagment.dao.impl;

import static com.epam.newsmanagment.utils.DBUtils.closeResources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;
import javax.xml.bind.annotation.W3CDomHandler;

import com.epam.newsmanagment.dao.ICommentDAO;
import com.epam.newsmanagment.entity.Comment;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.utils.DBUtils;

public class CommentDAOImpl implements ICommentDAO
{
    private DataSource dataSource;

    public CommentDAOImpl(DataSource dataSource)
    {
	super();
	this.dataSource = dataSource;
    }

    /**
     * Adds comment to database.
     *
     * @param news
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public Long addComment(Comment comment) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	Long returnId;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection
		    .prepareStatement("INSERT INTO comments (comment_id, news_id, comment_text, creation_date)"
		    	+ " VALUES (COMMENTS_SEQ.nextval, ?, ?, ?)", new String[] { "COMMENT_ID" });
	    ps.setLong(1, comment.getNewsId());
	    ps.setString(2, comment.getText());
	    ps.setTimestamp(3, comment.getCreationDate());
	    ps.executeUpdate();
	    
	    rs = ps.getGeneratedKeys();
	    if (!rs.next())
	    {
		throw new DAOException("Can't generate id for new news entity.");
	    }
	    returnId = rs.getLong(1);
	    comment.setId(returnId);
	    return returnId;
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in comment adding.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    /**
     * Deletes comment..
     * 
     * @param commentId
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public void deleteComment(Long commentId) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("DELETE FROM comments WHERE comment_id=?");
	    ps.setLong(1, commentId);
	    ps.executeUpdate();
	}
	catch (SQLException e)
	{
	     throw new DAOException("Error in comment deletion.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }
    
    @Override
    public Comment getComment(Long commentId) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("SELECT * FROM comments WHERE comment_id=?");
	    ps.setLong(1, commentId);
	    rs = ps.executeQuery();
	    rs.next();
	    return new Comment(rs.getLong("comment_id"), rs.getLong("news_id"), rs.getString("comment_text"),
		    rs.getTimestamp("creation_date"));
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in getting comment.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    @Override
    public List<Comment> getCommentsByNewsId(Long newsId) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    List<Comment> list = new ArrayList<Comment>();
	    
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("SELECT * FROM comments WHERE news_id=?");
	    ps.setLong(1, newsId);
	    rs = ps.executeQuery();
	    
	    while(rs.next())
	    {
		list.add(new Comment(rs.getLong("comment_id"), rs.getLong("news_id"), rs.getString("comment_text"),
			    rs.getTimestamp("creation_date")));
	    }
	    
	    return list;
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in getting comment.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }
}
