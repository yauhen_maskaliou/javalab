package com.epam.newsmanagment.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.newsmanagment.dao.ITagDAO;
import com.epam.newsmanagment.entity.Tag;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.exception.ServiceException;
import com.epam.newsmanagment.service.ITagService;

public class TagServiceImpl implements ITagService
{
    private ITagDAO tagDAO;
    private final static Logger LOG = Logger.getLogger(TagServiceImpl.class);
    
    public TagServiceImpl(ITagDAO tagDAO)
    {
	super();
	this.tagDAO = tagDAO;
    }
    
    @Override
    public void addNewsTagsRelations(Long newsId, List<Long> tagIdList) throws ServiceException
    {
	try
	{
	    tagDAO.addNewsTagsRelations(newsId, tagIdList);
	}
	catch (DAOException e)
	{
	    LOG.error("Erron in adding news-tag relations.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public Long addTag(Tag tag) throws ServiceException
    {
	try
	{
	    return tagDAO.addTag(tag);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not add tag.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public void updateTag(Tag tag) throws ServiceException
    {
	try
	{
	    tagDAO.updateTag(tag);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not update tag.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public Tag getTag(Long tagId) throws ServiceException
    {
	try
	{
	    return tagDAO.getTag(tagId);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not get tag.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public List<Tag> getAllTags() throws ServiceException
    {
	try
	{
	    return tagDAO.getAllTags();
	}
	catch (DAOException e)
	{
	    LOG.error("Can not get tag list.");
	    throw new ServiceException(e);
	}
    }

    @Override
    public void deleteTag(Long tagId) throws ServiceException
    {
	try
	{
	    tagDAO.deleteTag(tagId);
	    tagDAO.deleteNewsTagsRelationsByTagId(tagId);
	}
	catch (DAOException e)
	{
	    LOG.error("Can not delete tag.");
	    throw new ServiceException(e);
	}
    }
}
