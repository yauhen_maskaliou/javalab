package com.epam.newsmanagment.dao.impl;

import static com.epam.newsmanagment.utils.DBUtils.closeResources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.epam.newsmanagment.dao.IAuthorDAO;
import com.epam.newsmanagment.entity.Author;
import com.epam.newsmanagment.exception.DAOException;
import com.epam.newsmanagment.utils.DBUtils;

public class AuthorDAOImpl implements IAuthorDAO
{
    private DataSource dataSource;

    private final static Logger LOG = Logger.getLogger(AuthorDAOImpl.class);

    public AuthorDAOImpl(DataSource dataSource)
    {
	super();
	this.dataSource = dataSource;
    }

    /**
     * Adds news-author relation to database.
     *
     * @param newsId
     * @param authorId
     * @throws DAOException
     *             if any problems with accessing objects from database occurs.
     */
    @Override
    public void addNewsAuthorRelation(Long newsId, Long authorId) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("INSERT INTO news_authors (news_id, author_id) VALUES (?, ?)");
	    ps.setLong(1, newsId);
	    ps.setLong(2, authorId);
	    ps.executeUpdate();
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in adding news-author relation.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    @Override
    public void expireAuthor(Long authorId, Timestamp expirationDate) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("UPDATE authors SET expired=? WHERE author_id=?");
	    ps.setTimestamp(1, expirationDate);
	    ps.setLong(2, authorId);
	    ps.executeUpdate();
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in author expiring.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    @Override
    public Long addAuthor(Author author) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	Long returnId;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement(
		    "INSERT INTO authors (author_id, author_name) VALUES (authors_seq.nextval, ?)",
		    new String[] { "AUTHOR_ID" });
	    ps.setString(1, author.getName());
	    ps.executeUpdate();

	    rs = ps.getGeneratedKeys();
	    if (!rs.next())
	    {
		throw new DAOException("Can't generate id for new news entity.");
	    }
	    returnId = rs.getLong(1);
	    author.setId(returnId);
	    return returnId;
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in author adding.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    @Override
    public void updateAuthor(Author author) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("UPDATE authors SET author_name=? WHERE author_id=?");
	    ps.setString(1, author.getName());
	    ps.setLong(2, author.getId());
	    ps.executeUpdate();
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in author editing.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    @Override
    public Author getAuthor(Long authorId) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("SELECT * FROM authors WHERE author_id=?");
	    ps.setLong(1, authorId);
	    rs = ps.executeQuery();
	    rs.next();

	    Author author = new Author();
	    author.setId(authorId);
	    author.setName(rs.getString("author_name"));
	    author.setExpired(rs.getTimestamp("expired"));
	    return author;
	}
	catch (SQLException e)
	{

	    throw new DAOException("Error in getting author.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    @Override
    public Author getAuthorByNewsId(Long newsId) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("SELECT a1.author_id, a1.author_name " + "FROM news n "
		    + "INNER JOIN news_authors na1 ON n.news_id = na1.news_id "
		    + "INNER JOIN authors a1  ON a1.author_id = na1.author_id " + "WHERE n.news_id=?");
	    ps.setLong(1, newsId);
	    rs = ps.executeQuery();

	    if (!rs.next())
	    {
		return new Author();
	    }

	    return new Author(rs.getLong("author_id"), rs.getString("author_name"));
	}
	catch (SQLException e)
	{
	    LOG.error(e);
	    throw new DAOException("Error in getting author by news id.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    @Override
    public List<Author> getAllAuthors() throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("SELECT * FROM authors");
	    rs = ps.executeQuery();

	    List<Author> list = new ArrayList<Author>();

	    while (rs.next())
	    {
		Author author = new Author();
		author.setId(rs.getLong("author_id"));
		author.setName(rs.getString("author_name"));
		author.setExpired(rs.getTimestamp("expired"));
		list.add(author);
	    }

	    return list;
	}
	catch (SQLException e)
	{

	    throw new DAOException("Error in getting author.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }

    @Override
    public void updateNewsAuthorRelation(Long newsId, Long authorId) throws DAOException
    {
	Connection connection = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	try
	{
	    connection = DBUtils.getConnection(dataSource);
	    ps = connection.prepareStatement("UPDATE news_authors SET author_id=? WHERE news_id=?");
	    ps.setLong(1, authorId);
	    ps.setLong(2, newsId);
	    ps.executeUpdate();
	}
	catch (SQLException e)
	{
	    throw new DAOException("Error in updating news-author relation.", e);
	}
	finally
	{
	    closeResources(dataSource, connection, rs, ps);
	}
    }
}
