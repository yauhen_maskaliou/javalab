package com.epam.newsmanagment.entity;

import java.util.List;

public class DeleteList
{
    private List<Long> ids;

    public DeleteList()
    {
	
    }

    public DeleteList(List<Long> ids)
    {
	super();
	this.ids = ids;
    }

    public List<Long> getIds()
    {
	return ids;
    }

    public void setIds(List<Long> ids)
    {
	this.ids = ids;
    }
}
